import io from 'socket.io';


export default class WebSocketClient {

    constructor(uri = '/', {
                    path = '/',
                    forceNew = true,
                    transports = ['polling', 'websocket'],
                    autoConnect = false,
                } = {}
    ) {
        this.client = io(uri, {path, forceNew, transports, autoConnect});
    }

    connect() {
        this.client.open();
    }

    disconnect() {
        this.client.close();
    }

    receive(eventName, callback) {

        this.client.on(eventName, (data) => callback(transJson(data)));

    }

    send(eventName, data) {

        this.client.emit(eventName, data);

    }

}

/**

 * 将后台传过来的json字符串转换为object

 * @param data

 * @param callback

 */

function transJson(data) {

    let trans = data;

    if (typeof data === 'string' && (data.indexOf('{') === 0 || data.indexOf('[') === 0)) {

        trans = JSON.parse(data);

    }

    return trans;

}
