import React from 'react';

export interface IArticleHeaderProps {
  data: {
    content?: string;
    updatedAt?: any;
    avatar?: string;
    owner?: string;
    href?: string;
  };
}

export default class ArticleHeader extends React.Component<IArticleHeaderProps, any> {}
