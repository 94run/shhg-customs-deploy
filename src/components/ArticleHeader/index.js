import React, {Component} from 'react';
import moment from 'moment';
import {
    Avatar,
    Tag,
    Card,
    Col,
    Row,
    Icon,
    Descriptions,
    message,
    Form,
    Alert,
    Modal,
    Input,
    Checkbox,
    List,
} from 'antd';
import Link from 'umi/link';
import {connect} from 'dva';
import styles from './index.less';
import copy from 'copy-to-clipboard';

const FormItem = Form.Item;
const {TextArea} = Input;

const colorList = ['#f56a00', '#7265e6', '#ffbf00', '#00a2ae', '#87d068'];

const articleExtMap = {
    'enterprise': '企业',
    'brand': '品牌',
    'originArea': '原产地',
    'productName': '产品名称及描述',
    'riskItem': '不合格原因',
    'measure': '采取措施'
};

const renderDescriptionItems = (articleExt) => {
    if (articleExt === null) {
        articleExt = {
            enterprise: '',
            brand: '',
            originArea: '',
            productName: '',
            riskItem: '',
            measure: ''
        }
    }
    articleExt = (({enterprise, brand, originArea, productName, riskItem, measure}) => (
        {enterprise, brand, originArea, productName, riskItem, measure}))(articleExt);
    return Object.keys(articleExt).map(item => (
        <Descriptions.Item span={1} label={articleExtMap[item]}>{articleExt[item]}</Descriptions.Item>
    ))
};

const onClickShare = (id) => {
    let BASE_URL = window.location.href.replace(window.location.pathname, '');
    let url = BASE_URL + '/check/detail/' + id;
    copy(url);
    message.success('复制分享链接成功');
};

const cutFirstChar = (text) => {
    if (!text) {
        return '';
    }
    const first = text.substring(0, 1);
    if (text.length > 2 && first === '(' && text.indexOf(")") > 0) {
        return text.substring(1, text.indexOf(")"));
    }
    return first;
};

const getColor = (text) => {
    return colorList[text ? text.length % 5 : 0];
};

const CreateForm = Form.create()(props => {
    const {
        modalVisible, form, handleModalVisible, handleFormState,
        dispatch, articleId, formState, articleProperties, checkboxTypeList, handleResetCheckboxTypeList
    } = props;
    /**
     * 用户填写反馈内容点击确定
     * 向后端回传问题类型type和反馈内容content
     */
    const handleOk = () => {
        const content = form.getFieldValue('input-text');
        if (checkboxTypeList.length === 0 || content === undefined || content === '') {
            handleFormState(false);
        } else {
            let type = checkboxTypeList.join(';');
            dispatch({
                type: 'feedback/addFeedback',
                payload: {
                    type: type,
                    content: content,
                    articleId: articleId,
                }
            });
            handleModalVisible();
            handleFormState(true);
            handleResetCheckboxTypeList();
            message.success('反馈成功!');
        }
    };

    const renderMessage = content => (
        <Alert style={{marginBottom: 24}} message={content} type="error" showIcon d/>
    );

    /**
     * 根据用户点击复选框更改checkboxTypeList列表的值
     */
    const onChange = (value) => {
        if (-1 === checkboxTypeList.indexOf(value)) {
            checkboxTypeList.push(value);
        } else {
            checkboxTypeList.splice(checkboxTypeList.indexOf(value), 1);
        }
    };

    return (
        <Modal
            visible={modalVisible}
            mask={false}
            onOk={handleOk}
            onCancel={() => {
                handleModalVisible();
                handleFormState(true);
                handleResetCheckboxTypeList();
            }}
            title={<div style={{textAlign: "center"}}>反馈</div>}
            centered={true}
            destroyOnClose={true}
        >
            {!formState && renderMessage("问题类型未选择或反馈内容未输入!")}
            <FormItem>
                {form.getFieldDecorator('checkbox-group')(
                    <List
                        dataSource={articleProperties}
                        grid={{
                            gutter: 24,
                            row: Math.ceil(articleProperties && articleProperties.length / 3),
                            column: 3
                        }}
                        renderItem={item => (
                            <List.Item>
                                <Checkbox value={item.code}
                                          onChange={onChange.bind(this, item.code)}>{item.description}</Checkbox>
                            </List.Item>
                        )}
                    />
                )}
            </FormItem>
            <FormItem>
                {form.getFieldDecorator('input-text')(<TextArea rows="8" placeholder="请输入反馈信息"/>)}
            </FormItem>
        </Modal>
    );
});


@connect(({feedback, articles}) => ({
    feedback,
    articles
}))
export default class ArticleHeader extends Component {

    state = {
        modalVisible: false,
        articleId: 0,
        formState: [],
        checkboxTypeList: [],
        showContent: false
    };

    constructor(props){
        super(props);
        this.tableRef = React.createRef();
    }

    /**
     * 从后台PoPropertyEnum中加载反馈表单需要的属性值
     * 获取喜欢的列表
     */
    componentDidMount() {
    }

    handleModalVisible = (flag, articleId, disableOperate) => {
        if (disableOperate) {
            return;
        }
        this.setState({
            modalVisible: !!flag,
            articleId: articleId,
        });
    };


    isLike = (likeItem, articleId) => {
        return likeItem && likeItem.articleId === articleId;
    };

    // 处理点赞事件
    handleLike = (likeItem, articleId, disableOperate) => {
        if (disableOperate) {
            message.success("您已点赞");
            return;
        }
        const {dispatch} = this.props;
        if (this.isLike(likeItem, articleId)) {
            dispatch({
                type: 'articles/delLike',
                payload: articleId,
            });
        } else {
            dispatch({
                type: 'articles/addLike',
                payload: articleId,
            });
        }
    };


    /**
     * 控制modal中是否提示不满足条件文字
     * @param flag 表单状态为false时，提示文字
     */
    handleFormState = (flag) => {
        if (!flag) {
            this.setState({formState: false})
        } else {
            this.setState({formState: true})
        }
    };

    formatContent(content) {
        if (!content) {
            return "";
        }
        return content.replace(/(\r\n|\n|\r|\\n)/g, "<br/>");
    }

    /**
     * 反馈表单关闭时，重置checkboxTypeList
     */
    handleResetCheckboxTypeList = () => {
        this.setState({checkboxTypeList: []})
    };

    onClickShowContent = () => {
        let flag = this.state.showContent;
        this.setState({showContent: !flag})
    };

    render() {

        const {data: {title, summary, articleUrl, site, gmtCreate, keywords, articleId, likes, articleType, articleExt, displayType}, articleProperties, likeItem, disableOperate} = this.props;

        const {showContent} = this.state;

        const feedbackParams = {
            handleModalVisible: this.handleModalVisible,
            dispatch: this.props.dispatch,
            articleId: this.state.articleId,
            handleFormState: this.handleFormState,
            articleProperties: articleProperties,
            formState: this.state.formState,
            // onArticlePropertiesChange: this.onArticlePropertiesChange(articleProperties),
            checkboxTypeList: this.state.checkboxTypeList,
            handleResetCheckboxTypeList: this.handleResetCheckboxTypeList
        };

        return (
            <Card bordered={false} style={{marginBottom: 24}}>
            <Col span={4} className={styles.articleTitle}>
                {site && site.logo && <Avatar src={site.logo} alt='A'/>}
                {site && !site.logo && <Avatar
                    style={{backgroundColor: getColor(site.name)}}
                    size={64}>{cutFirstChar(site.name)}</Avatar>}
                <br/>
                {
                    <a className={styles.listItemMetaTitle} href={site && site.url}
                       target='_blank'>{site ? site.name : ""}</a>}
            </Col>
            <Col span={20}>
                <Row>
                    <Row gutter={24}>
                        <Col span={18}>
                            <Link className={styles.listItemMetaTitle} to={'/check/detail/' + articleId}>{title}</Link>
                        </Col>
                        <Col span={6} className={styles.operate}>
                            <em>{moment(gmtCreate).format('YYYY-MM-DD HH:mm')}</em>
                        </Col>
                    </Row>
                    <Row style={{marginBottom: '12px', marginTop: '12px'}}>
                        {/*非预警召回类有summary，summary无表格or预警召回类，但是displayType是summary，正常显示summary内容*/}
                        {((summary && summary.indexOf('<table') < 0 && articleType !== 'yjhzh') || (articleType === 'yjhzh' && displayType === 'summary')) &&
                        <div className={styles.listContent}>
                            <Link className={styles.articleSummary} to={'/check/detail/' + articleId}>
                                <div className={styles.description} dangerouslySetInnerHTML={{
                                    __html: this.formatContent(summary),
                                }}/>
                            </Link>
                        </div>}
                        {/*非预警召回类有summary，summary中有表格or预警召回类，但是displayType是table，显示表格，大于一定长度下拉显示隐藏*/}
                        {((summary && summary.indexOf('<table') >= 0 && articleType !== 'yjhzh') || (articleType === 'yjhzh' && displayType === 'table')) &&
                        <div className={styles.listContent}>
                            <Link className={styles.articleSummary} to={'/check/detail/' + articleId}>
                                <div
                                    ref={this.tableRef}
                                    className={`${styles.docTbStyle} 
                                    ${(this.tableRef.current && this.tableRef.current.offsetHeight >=227) ? 
                                        (showContent ? styles.fadeIn : styles.fadeOut): ''} `}
                                    dangerouslySetInnerHTML={{
                                    __html: summary,
                                }}/>
                            </Link>
                            {!showContent && this.tableRef.current &&
                            this.tableRef.current.offsetHeight >=227 && <Icon
                                className={styles.icon}
                                type="down"
                                onClick={this.onClickShowContent}/>}
                            {showContent && this.tableRef.current &&
                            this.tableRef.current.offsetHeight >=227 && <Icon
                                className={styles.icon}
                                type="up"
                                onClick={this.onClickShowContent}/>}
                        </div>}
                        {/*预警召回类，displayType是extract，显示六大项表格*/}
                        {articleType === 'yjhzh' && displayType === 'extract' &&
                        <Descriptions column={1} bordered size="small" className={styles.customsDescriptions}>
                            {renderDescriptionItems(articleExt)}
                        </Descriptions>}
                    </Row>
                    <Row className={styles.LineHeight}>
                        <Col span={18} >
                            <a href={articleUrl} target="_blank">查看原文</a>&emsp;&emsp;
                            {
                                keywords && keywords.map(k => <Tag key={k}>{k}</Tag>)
                            }
                        </Col>
                        <Col span={6} className={styles.operate}>
                            <Icon
                                type="warning"
                                onClick={this.handleModalVisible.bind(this, true, articleId, disableOperate)}
                            />&emsp;&emsp;
                            <Icon
                                type="share-alt"
                                onClick={onClickShare.bind(this, articleId)}
                            />&emsp;&emsp;
                            <Icon type="like" style={{marginRight: "8px"}}
                                  disable
                                  theme={this.isLike(likeItem, articleId) || disableOperate ? 'filled' : ''}
                                  className={this.isLike(likeItem, articleId) || disableOperate ? styles.isActive : null}
                                  onClick={this.handleLike.bind(this, likeItem, articleId, disableOperate)}/>{likes}
                        </Col>
                    </Row>
                </Row>
            </Col>
            <CreateForm {...feedbackParams} modalVisible={this.state.modalVisible}/>
        </Card>)
    }


}

