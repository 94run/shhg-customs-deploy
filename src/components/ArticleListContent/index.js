import React from 'react';
import moment from 'moment';
import { Avatar, Tag} from 'antd';
import styles from './index.less';

const ArticleListContent = ({ data: { title,content, summary, updatedAt, avatar, owner, href } }) => (
  <div className={styles.listContent}>
    <div className={styles.description}>{summary}</div>
  </div>
);

export default ArticleListContent;
