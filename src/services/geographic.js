import {axiosGet} from "./api";

export async function queryProvince() {
  return axiosGet('api/geographic/province');
}

export async function queryCity(province) {
  return axiosGet(`api/geographic/city/${province}`);
}
