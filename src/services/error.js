import {axiosGet} from "./api";

export default async function queryError(code) {
  return axiosGet(`api/${code}`);
}
