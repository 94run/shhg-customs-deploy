import {stringify} from 'qs';
import request from '@/utils/request';
import $$ from "cmn-utils/lib";
import router from 'umi/router';
import axios from 'axios';

let BASE_URL = "http://api.dataint.cn:8888/";

if (process.env.NODE_ENV === 'development') {
    BASE_URL = "http://127.0.0.1:8088/"
}


export function axiosGet(url, params) {
    return axios.get(
        BASE_URL + url,
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=utf-8',
                'token': $$.getStore('token')
            },
            params: {
                ...params
            }
        }
    ).catch(function (error) {
        if (error.response){
            if (506 === error.response.status){
                router.push( '/user/login')
            }
        }
    });
}

export function axiosPost(url, params) {
    if (url.indexOf('/login') > -1) {
        return axios.post(
            BASE_URL + url,
            {
                ...params
            },
            {
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json; charset=utf-8',
                }
            })
    }

    return axios.post(
        BASE_URL + url,
        {
            ...params
        },
        {
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json; charset=utf-8',
                'token': $$.getStore('token')
            }
        }
    ).catch(function (error) {
        if (error.response){
            if (506 === error.response.status){
                router.push( '/user/login')
            }
        }
    });
}

export function axiosPut(url, params) {
    return axios.put(
        BASE_URL + url,
        {
            ...params
        },
        {
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json; charset=utf-8',
                'token': $$.getStore('token')
            },
        }
    ).catch(e => function () {
        console.error(e);
    });
}

export function axiosDelete(url) {
    return axios.delete(
        BASE_URL + url,
        {
            ...params
        },
        {
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json; charset=utf-8',
                'token': $$.getStore('token')
            },
        }
    ).catch(e => function () {
        console.error(e);
    });
}

export async function queryHotArticles(params) {
    return axiosGet('data/queryHotArticles', params)
}

export async function queryArticle(params) {
    return axiosGet('article/detail/' + params.articleId + "/" + params.fromBack)
}

export async function queryByConditions(params) {
    return axiosGet('article/query', params)
}

export async function queryArticleTypes(params) {
    return axiosGet('type/articleTypes/', params)
}

export async function queryFeedbackTypeList() {
    return axiosGet('type/articleProperties/');
}

export async function queryFakeListOne(params) {
    return request(`api/fake_list_one?${stringify(params)}`);
}

export async function removeFakeList(params) {
    const {count = 5, ...restParams} = params;
    return request(`api/fake_list?count=${count}`, {
        method: 'POST',
        data: {
            ...restParams,
            method: 'delete',
        },
    });
}

export async function addFakeList(params) {
    const {count = 5, ...restParams} = params;
    return request(`api/fake_list?count=${count}`, {
        method: 'POST',
        data: {
            ...restParams,
            method: 'post',
        },
    });
}

export async function updateFakeList(params) {
    const {count = 5, ...restParams} = params;
    return request(`api/fake_list?count=${count}`, {
        method: 'POST',
        data: {
            ...restParams,
            method: 'update',
        },
    });
}

export async function login(params) {
    return axiosPost('user/login', params);
}

export async function queryCountries(params) {
    return axiosGet('type/countries', params);
}

export async function queryProductTypes(params) {
    return axiosGet('type/productTypes', params);
}

export async function queryMeasures(params) {
    return axiosGet('type/measures', params);
}

export async function queryRiskItems(params) {
    return axiosGet('type/riskItems', params);
}

export async function queryNotices(params = {}) {
    return request(`api/notices?${stringify(params)}`);
}

export async function getFakeCaptcha(mobile) {
    return request(`api/captcha?mobile=${mobile}`);
}

export async function queryReportList(params) {
    return axiosGet('report/queryReportList', params);
}

export async function queryLastWeekReport() {
    return axiosGet('report/queryLastWeekReport');
}

export async function queryThisWeekReport() {
    return axiosGet('report/queryThisWeekReport');
}

export async function downloadReport(params) {
    return axiosGet('report/download/' + params.reportId);
}

export async function queryReportDetail(params) {
    const {reportId} = params;
    return request(`api/data/queryReportDetail?reportId=${reportId}`, {
        method: 'GET'
    });
}

export async function queryLikeList(params) {
    return axiosGet(`api/data/queryLikeList`, params);
}

export async function queryLikesByArticleIds(params) {
    return axiosGet(`api/data/queryLikesByArticleIds`, params);
}


export async function queryFeedbackList(params) {
    return axiosGet('api/data/queryFeedbackList', params);
}

export async function addFeedback(params) {
    return axiosPost('feedback/addFeedback', params);
}

export async function queryEverydayData(params) {
    return axiosGet('data/queryEverydayData', params);
}

export async function queryKeywordCloud(params) {
    return axiosGet('keyword/cloud/'+params);
}

export async function queryHotCountry(params) {
    return axiosGet('data/queryHotCountry', params);
}

export async function queryLike(articleId) {
    return axiosGet('api/data/queryLike/'+articleId);
}

export async function addLike(articleId) {
    return axiosGet('api/data/addLike/' + articleId);
}

export async function delLike(likeId) {
    return axiosGet('api/data/delLike/' + likeId);
}

export async function queryHotKeywords() {
    return axiosGet('keyword/hot/');
}

export async function connectSocket() {
    return axiosGet('article/socket/connect/1');
}



