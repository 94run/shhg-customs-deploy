import {axiosGet} from "./api";

export async function query() {
  return axiosGet('api/users');
}

export async function queryCurrent() {
  return axiosGet('user/current');
}
