import React, { Fragment } from 'react';
import { Layout, Icon } from 'antd';
import GlobalFooter from '@/components/GlobalFooter';

const { Footer } = Layout;
const FooterView = () => (
  <Footer style={{ padding: 0 }}>
    <GlobalFooter
      links={[
        {
          key: '首页',
          title: '首页',
          href: 'https://customs.dataint.cn',
          blankTarget: true,
        },
        {
          key: '跨境食品安全舆情监控系统',
          title: '跨境食品安全舆情监控系统',
          href: 'https://customs.dataint.cn',
          blankTarget: true,
        },
      ]}
      copyright={
        <Fragment>
          Copyright <Icon type="copyright" /> 2019 上海频波罗智能技术有限公司
        </Fragment>
      }
    />
  </Footer>
);
export default FooterView;
