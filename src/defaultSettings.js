module.exports = {
  "navTheme": "light",
  "primaryColor": "#1890FF",
  "layout": "topmenu",
  "contentWidth": "Fluid",
  "fixedHeader": false,
  "autoHideHeader": false,
  "fixSiderbar": false,
  "menu": {
    "disableLocal": false
  },
  "title": "跨境食品安全舆情监控系统",
  "pwa": true,
  "iconfontUrl": "",
  "collapse": true,
  "runtimeMenu": true,
};
