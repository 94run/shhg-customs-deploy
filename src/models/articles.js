import {queryLikeList, queryLikesByArticleIds, addLike, delLike, queryByConditions, queryArticle} from '@/services/api';
import {findLike, findPos} from "./list";

function selectArticleType(flag) {
    let type;
    switch (flag) {
        case 'center':
            type = 'saveArticles';
            break;
        case 'search':
            type = 'saveArticlesSearch';
            break;
        case 'topic':
            type = 'saveArticlesTopic';
            break;
    }
    return type;
}

function selectConditionType(condition) {
    let putContent;
    switch (condition.flag) {
        case 'center':
            putContent = {
                type: 'putConditionReducer',
                payload: {condition: condition}
            };
            break;
        case 'search':
            putContent = {
                type: 'putConditionSearchReducer',
                payload: {conditionSearch: condition}
            };
            break;
        case 'topic':
            putContent = {
                type: 'putConditionTopicReducer',
                payload: {conditionTopic: condition}
            };
            break;
    }
    return putContent;
}

export default {
    namespace: 'articles',

    state: {
        articles: [],
        articlesSearch: [],
        articlesTopic: [],
        pagination: {},
        paginationSearch: {},
        paginationTopic: {},
        likePJ: [],
        likeList: [],
        likeListSearch: [],
        likeMap: {},
        likeMapSearch: {},
        article: {},
        condition: {},
        conditionSearch: {},
        conditionTopic: {},
        newArticleNum: 0,
    },

    effects: {
        * queryByConditions({payload, callback}, {call, put}) {
            const response = yield call(queryByConditions, payload);
            yield put({
                type: selectArticleType(payload.flag),
                payload: response.data
            });
            if (callback) {
                callback(response.data.data);
            }
        },
        * queryArticle({payload, callback}, {call, put}) {
            const response = yield call(queryArticle, payload);
            yield put({
                type: 'saveArticle',
                payload: {
                    article: response.data.data,
                },
            });
            if (callback) callback(response.data.data);
        },
        * fetch({payload}, {call, put}) {
            const response = yield call(queryLikeList, payload);
            yield put({
                type: 'queryList',
                payload: response.data,
            });
        },
        * fetchDefault({payload}, {call, put}) {
            const response = yield call(queryLikeList, payload);
            yield put({
                type: 'queryList',
                payload: Array.isArray(response) ? response : [],
            });
        },
        //searchFlag待修改
        * queryLikesByArticleIds({payload, callback}, {call, put}) {
            const response = yield call(queryLikesByArticleIds, payload);
            yield put({
                type: payload.searchFlag ? 'queryLikeListSearchReducer' : 'queryLikeListReducer',
                payload: payload.searchFlag ? {likeListSearch: response.data.data} : {likeList: response.data.data},
            });
            if (callback) callback();
        },
        * addLike({payload}, {call, put}) {
            const response = yield call(addLike, payload);
            yield put({
                type: 'addLikeReduce',
                payload: response.data.data,
            })
        },
        * delLike({payload}, {call, put}) {
            const response = yield call(delLike, payload);
            yield put({
                type: 'delLikeReduce',
                payload: response.data.data,
            })
        },
        * putCondition({payload}, {put}) {
            yield put(selectConditionType(payload.condition))
        },
    },

    reducers: {
        queryList(state, {payload}) {
            return {
                ...state,
                list: payload.likeList,
                pagination: payload.pagination
            };
        },
        queryLikeListSearchReducer(state, {payload: {likeListSearch}}) {
            const likeMapSearch = {};
            likeListSearch.forEach(item => (likeMapSearch[item.articleId] = item));
            return {
                ...state,
                likeMapSearch: likeMapSearch
            }
        },
        queryLikeListReducer(state, {payload: {likeList}}) {
            const likeMap = {};
            likeList.forEach(item => (likeMap[item.articleId] = item));
            return {
                ...state,
                likeMap: likeMap
            }
        },
        addLikeReduce(state, {payload}) {
            if (!payload || !payload.likes) {
                return {
                    ...state,
                }
            }
            const {likes, article} = payload;
            const {articles} = state;
            const pos = findPos(article, articles);
            if (pos >= 0) {
                state['articles'].splice(pos, 1, payload.article);
                state['likeList'].push(likes);
            }

            if (article) {
                state['article'] = article;
            }

            if (likes) {
                state['likeMap'][likes.articleId] = likes;
            }

            const posSearch = findPos(payload.article, state['articlesSearch']);
            if (posSearch >= 0) {
                state['articlesSearch'].splice(posSearch, 1, payload.article);
                state['likeListSearch'].push(likes);
                state['likeMapSearch'][likes.articleId] = likes;
            }
            return {
                ...state,
            }
        },
        delLikeReduce(state, {payload}) {
            if (!payload || !payload.likes) {
                return {
                    ...state,
                }
            }
            const likes = payload.likes;
            const pos = findPos(likes, state['likeList']);
            const articlePos = findPos(payload.article, state['articles']);
            if (articlePos >= 0) {
                state['articles'].splice(articlePos, 1, payload.article);
                delete state['likeMap'][payload.article.articleId]
            }
            const posSearch = findPos(likes, state['likeListSearch']);
            const articlePosSearch = findPos(payload.article, state['articlesSearch']);
            if (articlePosSearch >= 0) {
                state['articlesSearch'].splice(articlePosSearch, 1, payload.article);
                delete state['likeMapSearch'][payload.article.articleId]
            }
            if (pos >= 0) {
                state['likeList'].splice(pos, 1);
            }
            if (posSearch >= 0) {
                state['likeListSearch'].splice(pos, 1);
            }
            return {
                ...state,
            }
        },
        saveArticles(state, {payload}) {
            return {
                ...state,
                articles: payload.data,
                pagination: payload.pagination
            };
        },
        saveArticlesSearch(state, {payload}) {
            return {
                ...state,
                articlesSearch: payload.data,
                paginationSearch: payload.pagination
            };
        },
        saveArticlesTopic(state, {payload}) {
            return {
                ...state,
                articlesTopic: payload.data,
                paginationTopic: payload.pagination
            };
        },
        saveArticle(state, {payload: article}) {
            return {
                ...state,
                ...article
            };
        },
        putConditionReducer(state, {payload: condition}) {
            return {
                ...state,
                ...condition
            }
        },
        putConditionSearchReducer(state, {payload: conditionSearch}) {
            return {
                ...state,
                ...conditionSearch
            }
        },
        putConditionTopicReducer(state, {payload: conditionTopic}) {
            return {
                ...state,
                ...conditionTopic
            }
        }
    },
};
