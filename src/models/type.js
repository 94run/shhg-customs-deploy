import {queryCountries, queryProductTypes, queryMeasures, queryRiskItems, queryArticleTypes, queryHotKeywords} from '@/services/api';
import {findPos} from '@/utils/utils';

export default {
    namespace: 'type',

    state: {
        countries: [],
        productTypes: [],
        measures: [],
        riskItems: [],
        articleTypes: [],
        articleTypesSearch: [],
        keywords: []
    },

    effects: {
        * queryCountries({payload}, {call, put}) {
            const response = yield call(queryCountries, payload);
            yield put({
                type: 'queryCountryReducer',
                payload: {countries: response.data.data},
            });
        },
        * queryProductTypes({payload}, {call, put}) {
            const response = yield call(queryProductTypes, payload);
            yield put({
                type: 'queryProductTypeReducer',
                payload: {productTypes: response.data.data},
            });
        },
        * queryArticleTypes({payload}, {call, put}) {
            const response = yield call(queryArticleTypes, payload);
            if ('search' === payload.flag) {
                yield put({
                    type: 'queryArticleTypeSearchReducer',
                    payload: {articleTypesSearch: response.data.data},
                });
            } else {
                yield put({
                    type: 'queryArticleTypeReducer',
                    payload: {articleTypes: response.data.data},
                });
            }
        },
        * queryHotKeywords({payload}, {call, put}) {
            const response = yield call(queryHotKeywords, payload);
            yield put({
                type: 'queryKeywordReducer',
                payload: {keywords: response.data.data},
            });
        },
        * queryMeasures({payload}, {call, put}) {
            const response = yield call(queryMeasures, payload);
            yield put({
                type: 'queryMeasureReducer',
                payload: {measures: response.data.data},
            });
        },
        * queryRiskItems({payload}, {call, put}) {
            const response = yield call(queryRiskItems, payload);
            yield put({
                type: 'queryRiskItemReducer',
                payload: {riskItems: response.data.data},
            });
        },
    },

    reducers: {
        queryReducer(state, {payload}) {
            const {data} = state;
            return {
                ...state,
                data: {
                    ...data,
                    list: payload.data,
                    pagination: payload.pagination
                }
            }
        },
        queryCountryReducer(state, {payload: countries}) {
            return {
                ...state,
                ...countries
            }
        },
        queryProductTypeReducer(state, {payload: productTypes}) {
            return {
                ...state,
                ...productTypes
            }
        },
        queryArticleTypeReducer(state, {payload: articleTypes}) {
            return {
                ...state,
                ...articleTypes
            }
        },
        queryArticleTypeSearchReducer(state, {payload: articleTypesSearch}) {
            return {
                ...state,
                ...articleTypesSearch
            }
        },
        queryMeasureReducer(state, {payload: measures}) {
            return {
                ...state,
                ...measures
            }
        },
        queryKeywordReducer(state, {payload: keywords}) {
            return {
                ...state,
                ...keywords
            }
        },
        queryRiskItemReducer(state, {payload: riskItems}) {
            return {
                ...state,
                ...riskItems
            }
        },
        addReducer(state, {payload: item}) {
            const {data} = state;
            data['list'].splice(0, 0, item.item);
            return {...state, data: data}
        },
        updateReducer(state, {payload: item}) {
            const {data} = state;
            const pos = findPos(item.item, data['list']);
            if (pos >= 0) {
                data['list'].splice(pos, 1, item.item);
            }
            return {...state, data: data}
        },
        removeReducer(state, {payload: item}) {
            const {data} = state;
            const pos = findPos(item.item, data['list']);
            if (pos >= 0) {
                data['list'].splice(pos, 1);
            }
            return {...state, data: data}
        }
    },
};
