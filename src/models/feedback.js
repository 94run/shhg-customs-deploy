import {queryFeedbackTypeList, addFeedback} from '@/services/api';
import {findPos} from '@/utils/utils';

export default {
    namespace: 'feedback',

    state: {
        articleProperties: []
    },

    effects: {
        * queryFeedbackTypeList({payload}, {call, put}) {
            const response = yield call(queryFeedbackTypeList, payload);
            yield put({
                type: 'queryFeedbackTypeListReducer',
                payload: {articleProperties: response.data.data},
            });
        },
        * addFeedback({payload}, {call, put}) {
            yield call(addFeedback, payload);
        },
    },

    reducers: {
        queryFeedbackTypeListReducer(state, {payload: articleProperties}) {
            return {
                ...state,
                ...articleProperties
            }
        }
    },
};
