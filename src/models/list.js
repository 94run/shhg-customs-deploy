import {
    queryFakeList,
    removeFakeList,
    addFakeList,
    updateFakeList,
    queryFakeListOne,
    queryHotArticles,
    queryArticle,
    queryByConditions
} from '@/services/api';

export default {
    namespace: 'list',

    state: {
        list: [],
        detail: {},
        articles: [],
        article: {},
        pagination: {},
        likeList: [],
        like: {},
    },

    effects: {
        * fetch({payload}, {call, put}) {
            const response = yield call(queryFakeList, payload);
            yield put({
                type: 'queryList',
                payload: Array.isArray(response) ? response : [],
            });
        },
        * fetchOne({payload}, {call, put}) {
            const response = yield call(queryFakeListOne, payload);
            yield put({
                type: 'queryListOne',
                payload: response
            });
        },
        * appendFetch({payload}, {call, put}) {
            const response = yield call(queryFakeList, payload);
            yield put({
                type: 'appendList',
                payload: Array.isArray(response) ? response : [],
            });
        },
        * submit({payload}, {call, put}) {
            let callback;
            if (payload.id) {
                callback = Object.keys(payload).length === 1 ? removeFakeList : updateFakeList;
            } else {
                callback = addFakeList;
            }
            const response = yield call(callback, payload); // post
            yield put({
                type: 'queryList',
                payload: response,
            });
        },
        * queryByConditions({payload, callback}, {call, put}) {
            const response = yield call(queryByConditions, payload);
            yield put({
                type: 'saveArticles',
                payload: response.data,
            });
            if (callback) callback(response.data.data);
        }
    },

    reducers: {
        queryList(state, action) {
            return {
                ...state,
                list: action.payload,
            };
        },
        queryListOne(state, action) {
            return {
                ...state,
                detail: action.payload,
            };
        },
        appendList(state, action) {
            return {
                ...state,
                list: state.list.concat(action.payload),
            };
        },
        saveArticles(state, {payload}) {
            return {
                ...state,
                articles: payload.data,
                pagination: payload.pagination
            };
        },
    },
};

const matchKeyMap = ['articleId'];

export function findPos(item, dataList) {
  for (let i = 0, len = dataList.length; i < len; i++) {
    for (const p in matchKeyMap) {
      const key = matchKeyMap[p];
      if (item[key] != null && item[key] === dataList[i][key]) {
        return i;
      }
    }
  }
  return -1;
}

export function findLike(likeId,likeList) {
    for (let index = 0; index < likeList.length; index++) {
        const e_id = likeList[index].likeId;
        if(e_id != null && e_id === likeId) {
            return index;
        }
    }
    return -1;
}
