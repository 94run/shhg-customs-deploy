import defaultSettings from './defaultSettings';

export const dva = {
    config: {
        onError(err) {
            err.preventDefault();
        },
    },
};


export function render(oldRender) {
    if (defaultSettings.runtimeMenu) {
        // fetch('/api/auth_routes')
        //   .then(res => res.json())
        //   .then(ret => {
        //     authRoutes = ret;
        oldRender();
        //   });
    } else {
        oldRender();
    }
}


// export function patchRoutes(routes) {
//   if (defaultSettings.runtimeMenu) {
//     const routesRender = renderRoutes(authRoutes); // 方法自写
//     routes.length = 0; // eslint-disable-line
//     Object.assign(routes, routesRender);
//   }
// }
