import React, {memo} from 'react';
import {Row, Col, Table, Tooltip, Card, Icon} from 'antd';
import {FormattedMessage} from 'umi-plugin-react/locale';
import Trend from '@/components/Trend';
import numeral from 'numeral';
import styles from './Index.less';
import NumberInfo from '@/components/NumberInfo';
import {MiniArea} from '@/components/Charts';

const columns = [
    {
        title: '排名',
        dataIndex: 'ranking',
        key: 'ranking',
    },
    {
        title: '标题',
        dataIndex: 'title',
        key: 'title',
        render: (text, record) => <a href={"/check/detail/" + record.articleId}>{text}</a>,
    },
    {
        title: '热度',
        dataIndex: 'heatIndex',
        key: 'heatIndex',
        className: styles.alignRight,
    },
];

const HotArticles = memo(({loading, hotArticles}) => (
    <Card
        loading={loading}
        bordered={false}
        title='热点舆情（一周数据）'
        style={{marginTop: 24}}
    >
        <Table
            rowKey={record => record.articleId}
            size="small"
            columns={columns}
            dataSource={hotArticles}
            pagination={false}
        />
    </Card>
));

export default HotArticles;
