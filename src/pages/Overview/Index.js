import React, {Component, Suspense} from 'react';
import {connect} from 'dva';
import {Row, Col, Icon, Menu, Dropdown} from 'antd';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import {getTimeDistance} from '@/utils/utils';
import styles from './Index.less';
import router from 'umi/router';
import $$ from 'cmn-utils';
// import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import PageLoading from '@/components/PageLoading';
import DataSet from "@antv/data-set";


import {
    G2,
    Chart,
    Geom,
    Axis,
    Tooltip,
    Coord,
    Label,
    Legend,
    View,
    Guide,
    Shape,
    Facet,
    Util
} from "bizcharts";

const HotArticles = React.lazy(() => import('./HotArticles'));
const KeywordCloud = React.lazy(() => import('./KeywordCloud'));
const EverydayUpdate = React.lazy(() => import('./EverydayUpdate'));
const HotCountry = React.lazy(() => import('./HotCountry'));

@connect(({chart, overview, loading}) => ({
    chart,
    overview,
    everydayLoading: loading.effects['overview/queryEverydayData'],
    keywordLoading: loading.effects['overview/queryKeywordCloud'],
    countryLoading: loading.effects['overview/queryHotCountry'],
    articleLoading: loading.effects['overview/queryHotArticles'],
}))
class Index extends Component {
    state = {
        salesType: 'all',
        currentTabKey: '',
        dateType: 'week',
        rangePickerValue: getTimeDistance('year'),
    };

    componentDidMount() {

        const token = $$.getStore('token');
        if (!token) {
            router.push('/user/login');
            return;
        }

        const {dispatch} = this.props;
        this.reqRef = requestAnimationFrame(() => {
            dispatch({
                type: 'chart/fetch',
            });
        });
        dispatch({
            type: 'overview/queryHotArticles',
            payload: {pageSize: 13, currentPage: 1}
        });
        dispatch({
            type: 'overview/queryEverydayData',
        });
        dispatch({
            type: 'overview/queryKeywordCloud',
            payload: "week"
        });
        dispatch({
            type: 'overview/queryHotCountry',
        });
    }

    componentWillUnmount() {
        const {dispatch} = this.props;
        dispatch({
            type: 'chart/clear',
        });
        cancelAnimationFrame(this.reqRef);
    }

    handleChangeSalesType = e => {
        this.setState({
            salesType: e.target.value,
        });
    };

    handleTabChange = key => {
        this.setState({
            currentTabKey: key,
        });
    };

    handleRangePickerChange = rangePickerValue => {
        const {dispatch} = this.props;
        this.setState({
            rangePickerValue,
        });

        dispatch({
            type: 'chart/fetchSalesData',
        });
    };

    isActive = type => {
        const {dateType} = this.state;
        if (
            type === dateType
        ) {
            return styles.currentDate;
        }
        return '';
    };

    selectDate = dateType => {
        const {dispatch} = this.props;
        this.setState({
            dateType: dateType,
        });

        dispatch({
            type: 'overview/queryKeywordCloud',
            payload: dateType
        });
    };

    changeEverydayUpdateData(list) {
        if (list === undefined) {
            list = [];
        }
        list.map(item => {
            item.value = parseInt(item.value);
            return item
        })
    }


    render() {
        const {rangePickerValue, salesType, currentTabKey} = this.state;
        const {chart, keywordLoading, countryLoading, everydayLoading, articleLoading, overview: {keywordData, countryData, hotArticles}} = this.props;
        let {overview: {everydayUpdateData}} = this.props;
        this.changeEverydayUpdateData(everydayUpdateData);
        const {
            offlineData,
            offlineChartData,
            salesTypeData,
            salesTypeDataOnline,
            salesTypeDataOffline,
        } = chart;
        let salesPieData;
        if (salesType === 'all') {
            salesPieData = salesTypeData;
        } else {
            salesPieData = salesType === 'online' ? salesTypeDataOnline : salesTypeDataOffline;
        }
        const menu = (
            <Menu>
                <Menu.Item>操作一</Menu.Item>
                <Menu.Item>操作二</Menu.Item>
            </Menu>
        );

        const dropdownGroup = (
            <span className={styles.iconGroup}>
        <Dropdown overlay={menu} placement="bottomRight">
          <Icon type="ellipsis"/>
        </Dropdown>
      </span>
        );

        const activeKey = currentTabKey || (offlineData[0] && offlineData[0].name);


        let dv = [];

        function getTextAttrs(cfg) {
            return _.assign(
                {},
                cfg.style,
                {
                    fillOpacity: cfg.opacity,
                    fontSize: cfg.origin._origin.size,
                    // rotate: cfg.origin._origin.rotate,
                    text: cfg.origin._origin.text,
                    textAlign: "center",
                    fontFamily: cfg.origin._origin.font,
                    fill: cfg.color,
                    textBaseline: "Alphabetic"
                }
            );
        } // 给point注册一个词云的shape

        Shape.registerShape("point", "cloud", {
            drawShape(cfg, container) {
                const attrs = getTextAttrs(cfg);
                return container.addShape("text", {
                    attrs: _.assign(attrs, {
                        x: cfg.x,
                        y: cfg.y
                    })
                });
            }
        });
        if (keywordData && keywordData.length > 0) {
            dv = new DataSet.View().source(keywordData);
            const range = dv.range("heatIndex");
            const min = range[0];
            const max = range[1];
            dv.transform({
                type: "tag-cloud",
                fields: ["name", "heatIndex"],
                size: [window.innerWidth, window.innerHeight],
                font: "Verdana",
                padding: 0,
                timeInterval: 5000,

                // max execute time
                rotate() {
                    let random = ~~(Math.random() * 4) % 4;

                    if (random == 2) {
                        random = 0;
                    }

                    return random * 90; // 0, 90, 270
                },

                fontSize(d) {
                    if (d.value) {
                        const divisor = (max - min) !== 0 ? (max - min) : 1;
                        return ((d.value - min) / divisor) * (80 - 24) + 24;
                    }

                    return 0;
                }
            });
            const scale = {
                x: {
                    nice: false
                },
                y: {
                    nice: false
                }
            };

        }

        return (
            <GridContent>
                <div className={styles.twoColLayout}>
                    <Row gutter={24}>
                        <Col xl={12} lg={24} md={24} sm={24} xs={24}>
                            <Suspense fallback={null}>
                                <HotCountry
                                    loading={countryLoading}
                                    selectDate={this.selectDate}
                                    countryData={countryData}
                                    dropdownGroup={dropdownGroup}
                                />
                            </Suspense>
                        </Col>
                        <Col xl={12} lg={24} md={24} sm={24} xs={24}>
                            <Suspense fallback={null}>
                                <EverydayUpdate
                                    activeKey={activeKey}
                                    loading={everydayLoading}
                                    everydayUpdateData={everydayUpdateData}
                                    offlineChartData={offlineChartData}
                                    handleTabChange={this.handleTabChange}
                                />
                            </Suspense>
                        </Col>
                    </Row>
                    <Row gutter={24}>
                        <Col xl={12} lg={24} md={24} sm={24} xs={24}>
                            <Suspense fallback={null}>
                                <HotArticles
                                    loading={articleLoading}
                                    selectDate={this.selectDate}
                                    hotArticles={hotArticles}
                                    dropdownGroup={dropdownGroup}
                                />
                            </Suspense>
                        </Col>
                        <Col xl={12} lg={24} md={24} sm={24} xs={24}>
                            <Suspense fallback={null}>
                                <KeywordCloud
                                    activeKey={activeKey}
                                    selectDate={this.selectDate}
                                    isActive={this.isActive}
                                    loading={keywordLoading}
                                    keywordData={dv}
                                    handleTabChange={this.handleTabChange}
                                />
                            </Suspense>
                        </Col>
                    </Row>
                </div>
            </GridContent>
        );
    }
}

export default Index;
