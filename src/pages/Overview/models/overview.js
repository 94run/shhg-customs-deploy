import {queryEverydayData, queryKeywordCloud, queryHotCountry, queryHotArticles} from '@/services/api';

export default {
    namespace: 'overview',

    state: {
        visitData: [],
        visitData2: [],
        salesData: [],
        searchData: [],
        hotArticles: [],
        offlineData: [],
        offlineChartData: [],
        salesTypeData: [],
        salesTypeDataOnline: [],
        salesTypeDataOffline: [],
        everydayUpdateData: [],
        keywordData: [],
        mapData: {"features": []},
        countryData: [],
        radarData: [],
        loading: false,
    },

    effects: {
        * queryEverydayData({payload}, {call, put}) {
            const response = yield call(queryEverydayData, payload);
            yield put({
                type: 'saveReducer',
                payload: {
                    everydayUpdateData: response.data.data,
                },
            });
        },
        * queryKeywordCloud({payload}, {call, put}) {
            const response = yield call(queryKeywordCloud, payload);
            yield put({
                type: 'saveKeywordReducer',
                payload: {
                    keywordData: response.data.data,
                },
            });
        },
        * queryHotCountry({payload}, {call, put}) {
            const response = yield call(queryHotCountry, payload);
            yield put({
                type: 'saveHotCountryReducer',
                payload: {
                    countryData: response.data.data,
                },
            });
        },
         * queryHotArticles({payload}, {call, put}) {
            const response = yield call(queryHotArticles, payload);
            yield put({
                type: 'save',
                payload: {
                    hotArticles: response.data.data,
                },
            });
        },
    },

    reducers: {
        saveReducer(state, {payload: everydayUpdateData}) {
            return {
                ...state,
                ...everydayUpdateData,
            };
        },
        saveKeywordReducer(state, {payload: keywordData}){
            return {
                ...state,
                ...keywordData
            }
        },
        saveHotCountryReducer(state, {payload: countryData}){
            return {
                ...state,
                ...countryData
            }
        },
        save(state, {payload}) {
            return {
                ...state,
                ...payload,
            };
        },
        clear() {
            return {
                visitData: [],
                visitData2: [],
                salesData: [],
                searchData: [],
                offlineData: [],
                offlineChartData: [],
                salesTypeData: [],
                salesTypeDataOnline: [],
                salesTypeDataOffline: [],
                radarData: [],
            };
        },
    },
};
