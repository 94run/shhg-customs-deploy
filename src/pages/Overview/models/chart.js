import {fakeChartData, queryHotPos} from '@/services/api';

export default {
    namespace: 'chart',

    state: {
        visitData: [],
        visitData2: [],
        salesData: [],
        searchData: [],
        hotArticles: [],
        offlineData: [],
        offlineChartData: [],
        salesTypeData: [],
        salesTypeDataOnline: [],
        salesTypeDataOffline: [],
        everydayUpdateData: [],
        keywordData: [],
        mapData: {"features": []},
        countryData: [],
        radarData: [],
        loading: false,
    },

    effects: {
        * fetch(_, {call, put}) {
            const response = yield call(fakeChartData);
            yield put({
                type: 'save',
                payload: response,
            });
        },
        * fetchSalesData(_, {call, put}) {
            const response = yield call(fakeChartData);
            yield put({
                type: 'save',
                payload: {
                    salesData: response.data,
                },
            });
        },
    },

    reducers: {
        save(state, {payload}) {
            return {
                ...state,
                ...payload,
            };
        },
        clear() {
            return {
                visitData: [],
                visitData2: [],
                salesData: [],
                searchData: [],
                offlineData: [],
                offlineChartData: [],
                salesTypeData: [],
                salesTypeDataOnline: [],
                salesTypeDataOffline: [],
                radarData: [],
            };
        },
    },
};
