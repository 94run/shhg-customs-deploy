import React, {memo} from 'react';
import {Card, Tabs, Row, Col, DatePicker} from 'antd';
import {formatMessage, FormattedMessage} from 'umi-plugin-react/locale';
import styles from './Index.less';
import {TimelineChart, Pie} from '@/components/Charts';
import NumberInfo from '@/components/NumberInfo';
import {
    G2,
    Chart,
    Geom,
    Axis,
    Tooltip,
    Coord,
    Label,
    Legend,
    View,
    Guide,
    Shape,
    Facet,
    Util
} from "bizcharts";

const {RangePicker} = DatePicker;
const {TabPane} = Tabs;


const KeywordCloud = memo(
    ({activeKey, loading, keywordData, selectDate, handleTabChange, isActive, rangePickerValue, handleRangePickerChange}) => (
        <Card title="关键词云" loading={loading} bordered={false} bodyStyle={{padding: 0}} style={{marginTop: 24}}
              extra={
                  <div className={styles.everydayExtraWrap}>
                      <div className={styles.salesExtra}>
                          <a className={isActive('week')} onClick={() => selectDate('week')}>
                              <FormattedMessage id="app.analysis.all-week" defaultMessage="All Week"/>
                          </a>
                          <a className={isActive('month')} onClick={() => selectDate('month')}>
                              <FormattedMessage id="app.analysis.all-month" defaultMessage="All Month"/>
                          </a>
                          <a className={isActive('year')} onClick={() => selectDate('year')}>
                              <FormattedMessage id="app.analysis.all-year" defaultMessage="All Year"/>
                          </a>
                      </div>
                  </div>
              }
              size="large"
        >
            <Chart
                height={575}
                width={window.innerWidth}
                data={keywordData}
                padding={0}
                forceFit
            >
                <Tooltip showTitle={false}/>
                <Coord reflect="y"/>
                <Geom
                    type="point"
                    position="x*y"
                    color="keywordId"
                    shape="cloud"
                    tooltip="value"
                />
            </Chart>
        </Card>
    )
);

export default KeywordCloud;
