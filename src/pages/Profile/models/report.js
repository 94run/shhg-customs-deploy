import {queryReportList, queryReportDetail, queryLastWeekReport, queryThisWeekReport, downloadReport} from '@/services/api';

export default {
    namespace: 'report',

    state: {
        reportLastWeek: [], //上周周报
        reportDailyThisWeek: [], //本周日报
        reportBefore: [], //往期的日报或周报
        reportDetail: [], //日报或周报的详情
        pagination: {}
    },

    effects: {
        *queryReportList({payload}, {call, put}) {
            const response = yield call(queryReportList, payload);
            yield put({
                type: 'saveReportBefore',
                payload: response.data,
            });
        },
        *queryLastWeekReport({payload}, {call, put}) {
            const response = yield call(queryLastWeekReport);
            yield put({
                type: 'saveLastWeek',
                payload: response.data.data,
            });
        },
        *queryThisWeekReport({payload}, {call, put}) {
            const response = yield call(queryThisWeekReport);
            yield put({
                type: 'saveThisWeek',
                payload: response.data.data,
            });
        },
        *fetchDetail({payload},{call, put}){
            const response = yield call(queryReportDetail, payload);
            yield put({
                type: 'show',
                payload: response,
            });
        },
        *downloadReport({payload}, {call, put}){
            const response = yield call(downloadReport, payload);
            yield put({
                type: 'download',
                payload: response,
            })
        }
    },

    reducers: {
        saveReportBefore(state, { payload }) {
            return {
                ...state,
                reportBefore: payload.data,
                pagination: payload.pagination
            };
        },
        saveLastWeek(state, { payload }) {
            return {
                ...state,
                reportLastWeek: payload,
            };
        },
        saveThisWeek(state, { payload }) {
            return {
                ...state,
                reportDailyThisWeek: payload,
            };
        },
        download (state, { payload}) {
            payload.blob().then(blob => {
                //关闭loading 按钮恢复正常
                this.setState({
                  loadingStatus: true,
                  buttonDisabled: false,
                });
                let blobUrl = window.URL.createObjectURL(blob);
                const filename = times.formatNowDate() + '.docx';
                const aElement = document.createElement('a');
                document.body.appendChild(aElement);
                aElement.style.display = 'none';
                aElement.href = blobUrl;
                aElement.download = filename;
                aElement.click();
                document.body.removeChild(aElement);
              });
           return {
               ...state
           }
        },
        clear() {
            return {
                reportLastWeek: [],
                reportDailyThisWeek: [],
                reportWeekBefore: [],
                reportDailyBefore: [],
                reportDetail: []
            };
        },
    }
}