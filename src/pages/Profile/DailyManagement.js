import React, { Component } from 'react';
import { List, Card, Row, Col, Divider, Select, Icon, message, Modal, Spin } from 'antd';
import router from "umi/router";
import { connect } from "dva";
import styles from "../Overview/Index.less";
// import FileViewer from 'react-file-viewer';
import mammoth from 'mammoth';
import moment from 'moment';


const Option = Select.Option;

@connect(({ report, loading }) => ({
    report,
    loading: loading.effects['report/fetch'],
}))
class DailyManagement extends Component {
    state = {
        reportType: "weekly",
        initYear: moment().year(),
        initMonth: moment().month()+ 1,
        year: moment().year(),
        month: moment().month()+ 1,
        fileType: '.docx',
        visible: false,
        reportItem: {},
    };

    onDailyReportChange = item => {

        const { year, month, fileType } = this.state

        let BASE_URL = "http://api.dataint.cn:8888/";

        if (process.env.NODE_ENV === 'development') {
        BASE_URL = "http://127.0.0.1:8088/"
    }

        if (item.docPath.split('.').pop() === 'docx') {
            const url = BASE_URL + 'report/download/' + item.reportId
            var xhr = null;
            if (window.XMLHttpRequest) {//Mozilla 浏览器
                xhr = new XMLHttpRequest();
            } else {
                if (window.ActiveXObject) {//IE 浏览器
                    try {
                        xhr = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    catch (e) {
                        try {//IE 浏览器
                            xhr = new ActiveXObject("Msxml2.XMLHTTP");
                        }
                        catch (e) {
                        }
                    }
                }
            }
            xhr.open('GET', url, true);
            // xhr.responseType = "arraybuffer";设置返回文件的类型
            const token = localStorage.getItem('token').slice(1, -1);
            xhr.responseType = 'arraybuffer'
            xhr.setRequestHeader("token", token);
            //post请求一定要添加请求头才行不然会报错
            xhr.setRequestHeader("Content-type", "application/json");
            xhr.send();
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    mammoth.convertToHtml(
                        { arrayBuffer: xhr.response },
                        { includeDefaultStyleMap: true },
                    )
                        .then((result) => {
                            const docEl = document.createElement('div');
                            docEl.className = `${styles['document-container']}`
                            docEl.id = 'docContnet'
                            docEl.innerHTML = result.value;
                            document.getElementById('docx').innerHTML = docEl.outerHTML;
                            let regDate = /(\d{4})年(\d{1,2})月(\d{1,2})日/
                            let regContent = /[\u4e00-\u9fa5]/g
                            const CustomName= document.querySelector('#docx > div > p:nth-child(3)').textContent.match(regContent).join("").replace('年','').replace('月', '').replace('日', '')
                            const dat = regDate.exec(document.querySelector('#docx > div > p:nth-child(3)').textContent)[0]
                            const tempCont = document.createElement('span')
                            const tempDat = document.createElement('span')
                            tempCont.innerHTML = CustomName
                            tempDat.innerHTML = dat
                            tempDat.style.float = 'right'
                            document.querySelector('#docx > div > p:nth-child(3)').innerHTML = ''
                            document.querySelector('#docx > div > p:nth-child(3)').insertAdjacentElement('beforeend', tempCont)
                            document.querySelector('#docx > div > p:nth-child(3)').insertAdjacentElement('beforeend', tempDat)
                        })
                        .catch((a) => {
                            console.log('alexei: something went wrong', a);
                        })
                        .done();
                }
            }
        } else {
            const fileType = item.docPath.split('.').pop()
            document.getElementById('docx').innerHTML = `暂不支持${fileType}文件格式预览，请直接下载！`
        }

        this.setState({
            visible: !this.state.visible,
            reportItem: item,
        })
    };

    componentDidMount() {
        const { dispatch } = this.props;
        this.setState({
            initYear: new Date().getFullYear(),
            initMonth: new Date().getMonth + 1
        }, () => {
            const { reportType, initYear, month } = this.state;
            dispatch({
                type: 'report/queryReportList',
                payload: {
                    currentPage: 1,
                    pageSize: 8,
                    type: reportType,
                    year: initYear,
                    month: month,
                }
            });
        })

        dispatch({
            type: 'report/queryLastWeekReport'
        });
        dispatch({
            type: 'report/queryThisWeekReport'
        })
    }

    onChangeType(type) {
        const { dispatch } = this.props;
        const { year, month } = this.state;

        dispatch({
            type: 'report/queryReportList',
            payload: {
                currentPage: 1,
                pageSize: 8,
                type: type,
                year: year,
                month: month,
            }
        });
        this.setState({
            reportType: type
        });
    }

    onChangeYear(year) {
        const { dispatch } = this.props;
        const { reportType, month } = this.state;
        dispatch({
            type: 'report/queryReportList',
            payload: {
                currentPage: 1,
                pageSize: 8,
                type: reportType,
                year: year,
                month: month,
            },
        });
        this.setState({
            year: year
        });
    }

    onChangeMonth(month) {
        const { dispatch } = this.props;
        const { reportType, year } = this.state;
        dispatch({
            type: 'report/queryReportList',
            payload: {
                currentPage: 1,
                pageSize: 8,
                type: reportType,
                year: year,
                month: month,
            },
        });
        this.setState({
            month: month
        });
    }

    generateYearList() {
        const startYear = 2020;
        const { initYear } = this.state;
        let yearList = [];
        if (startYear === initYear) {
            yearList.push(<Option key={initYear}>{initYear}年</Option>);
        } else {
            for (let i = startYear; i <= initYear; i++) {
                yearList.push(<Option key={i}>{i}年</Option>)
            }
        }
        return yearList;
    }

    generateMonthList() {
        const { month } = this.state;
        let monthList = [];
        for (let i = 1; i <= 12; i++) {
            monthList.push(<Option key={i}>{i}月</Option>)
        }
        return monthList;
    }

    handleListChange(page, pageSize) {
        const { reportType, year } = this.state;
        const { dispatch } = this.props;
        dispatch({
            type: 'report/queryReportList',
            payload: {
                currentPage: page,
                pageSize: pageSize,
                type: reportType,
                year: year
            }
        });
    }


    downloadFile = () => {
        const { year, month, reportItem, fileType } = this.state

        let BASE_URL = "http://api.dataint.cn:8888/";

        if (process.env.NODE_ENV === 'development') {
            BASE_URL = "http://127.0.0.1:8088/"
        }
        const url = BASE_URL + 'report/download/' + reportItem.reportId
        var xhr = null;
        if (window.XMLHttpRequest) {//Mozilla 浏览器
            xhr = new XMLHttpRequest();
        } else {
            if (window.ActiveXObject) {//IE 浏览器
                try {
                    xhr = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                    try {//IE 浏览器
                        xhr = new ActiveXObject("Msxml2.XMLHTTP");
                    }
                    catch (e) {
                    }
                }
            }
        }
        xhr.open('GET', url, true);
        // xhr.responseType = "blob";设置返回文件的类型
        const token = localStorage.getItem('token').slice(1, -1);
        xhr.responseType = 'blob'
        xhr.setRequestHeader("token", token);
        //post请求一定要添加请求头才行不然会报错
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.send();
        xhr.onload = function () {
            if (this.status == 200) {
                //一个字符串，表明该Blob对象所包含数据的MIME类型
                let blob = new Blob([this.response], { type: "application/msword" });// this.response也就是请求的返回就是Blob对象
                let a = document.createElement('a');
                let url = URL.createObjectURL(blob);
                a.href = url;
                document.body.append(a);
                a.download = year + '年' + month + '月' + reportItem.title + fileType;
                if (!!window.ActiveXObject || "ActiveXObject" in window) {
                    window.navigator.msSaveOrOpenBlob(blob, new Date().getFullYear() + '年' + (new Date().getMonth() + 1) + '月' + reportItem.title + fileType)
                } else {
                    a.click()
                }
                window.URL.revokeObjectURL(url);
            } else {
                message.error('暂无此资源！')
            }
        }
        this.setState({
            visible: !this.state.visible
        })
    }

    cancelDownload = () => {
        this.setState({
            visible: !this.state.visible
        })
    }

    render() {
        const { report, loading } = this.props;
        const { reportType, initMonth, initYear, visible } = this.state;
        const {
            reportLastWeek,
            reportDailyThisWeek,
            reportBefore,
            pagination
        } = report;

        const paginationProps = {
            current: pagination.current === undefined ? 1 : parseInt(pagination.current),
            pageSize: pagination.pageSize === undefined ? 8 : parseInt(pagination.pageSize),
            total: pagination.total === undefined ? 8 : parseInt(pagination.total),
            onChange: (page, pageSize) => {
                this.handleListChange(page, pageSize)
            }
        };

        return (
            <Card
                loading={loading}
                className={styles.offlineCard}
                bordered={false}
            >
                <Row gutter={24}>
                    <Col span={20} offset={2}>
                        <Row gutter={24}>
                            <Col span={3}>上周周报</Col>
                            <Col span={20} offset={1}>本周日报</Col>
                        </Row>
                        <Divider />
                        <Row gutter={24}>
                            <Col span={3}>
                                {reportLastWeek && reportLastWeek.length !== 0 && <Card hoverable={true}
                                    style={{ textAlign: 'center' }}
                                    bodyStyle={{ padding: '50px 0px 20px 0px' }}
                                    onClick={this.onDailyReportChange.bind(this, reportLastWeek)}>
                                    <Icon type="file" style={{ fontSize: "70px" }} theme="twoTone" />
                                    <br /><br />
                                    <strong>{reportLastWeek.title}</strong><br /><br /><div style={{ fontSize: '12px' }}>{moment(reportLastWeek.gmtDate.slice(5,10), 'MM-DD').add(-6, "day").format('MM-DD').replace('-','.') + '~'+ reportLastWeek.gmtDate.slice(5,10).replace('-','.')}</div>
                                </Card>}
                            </Col>
                            <Col span={20} offset={1}>
                                <List
                                    grid={{ gutter: 16, column: 7 }}
                                    dataSource={reportDailyThisWeek}
                                    renderItem={(item, index) => (
                                        <List.Item >
                                            <Card
                                                hoverable={true}
                                                key={index}
                                                style={{ textAlign: 'center' }}
                                                bodyStyle={{ padding: '50px 0px 20px 0px' }}
                                                onClick={this.onDailyReportChange.bind(this, item)}>
                                                <Icon type="file" style={{ fontSize: "70px" }} theme="twoTone" />
                                                <br /><br />
                                                <strong>{item.title}</strong><br /><br /><div style={{ fontSize: '12px' }}>{item.gmtDate.slice(0, 10)}</div>
                                            </Card>
                                        </List.Item>
                                    )}
                                />
                            </Col>
                        </Row>
                        <Divider />
                        <Row gutter={24}>
                            <Col span={2}>
                                <Select defaultValue={initYear + '年'} style={{ width: 120 }} onChange={this.onChangeYear.bind(this)} >
                                    {this.generateYearList().reverse()}
                                </Select>
                            </Col>
                            <Col span={2} offset={1}>
                                <Select defaultValue={initMonth + '月'} style={{ width: 120 }} onChange={this.onChangeMonth.bind(this)}>
                                    {this.generateMonthList().reverse()}
                                </Select>
                            </Col>
                            <Col span={2} offset={1}>
                                <Select defaultValue='周报' style={{ width: 120 }} onChange={(value) => this.onChangeType(value)}>
                                    <Option key='weekly'>周报</Option>
                                    <Option key='daily'>日报</Option>
                                </Select>
                            </Col>
                        </Row>
                        <Divider />
                        <Row>
                            <List
                                grid={{ gutter: 16, column: 7 }}
                                dataSource={reportBefore}
                                pagination={paginationProps}
                                renderItem={(item, index) => (
                                    <List.Item >
                                        <Card hoverable={true}
                                            key={index}
                                            style={{ textAlign: 'center' }}
                                            bodyStyle={{ padding: '50px 0px 50px 0px' }}
                                            onClick={this.onDailyReportChange.bind(this, item)}>
                                            <Icon type="file" style={{ fontSize: "70px" }} theme="twoTone" />
                                            <br /><br />
                                            <strong>{item.title}</strong><br /><br /><div style={{ fontSize: '12px' }}>{reportType === 'weekly' ? moment(item.gmtDate.slice(5,10), 'MM-DD').add(-6, "day").format('MM-DD').replace('-','.') + '~'+ item.gmtDate.slice(5,10).replace('-','.'):item.gmtDate.slice(0, 10)}</div>
                                        </Card>
                                    </List.Item>
                                )}
                            />
                        </Row>
                        <Modal
                            title="日报详情"
                            style={{ top: 20 }}
                            visible={visible}
                            okText="下载"
                            width={1000}
                            onOk={() => this.downloadFile()}
                            onCancel={() => this.cancelDownload()}
                        >
                            {/* <div key={fileUrl}>
                                {fileUrl !== '' && <FileViewer
                                    fileType={fileType}
                                    filePath={fileUrl}/>}
                            </div> */}
                            <div className={styles['pg-viewer-wrapper']} >
                                <div className={styles['pg-viewer']} id="pg-viewer">
                                    <div id="docx" className={styles['docx']}>
                                        <Spin />
                                    </div>
                                </div>
                            </div>

                        </Modal>
                    </Col>
                </Row>
            </Card>
        );
    }
}


export default DailyManagement;
