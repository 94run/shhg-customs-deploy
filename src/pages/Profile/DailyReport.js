import React,{Component} from "react";
import { Row, Col, Button, Divider, Card, List, Table} from 'antd';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import moment from 'moment';
import router from "umi/router";
import {connect} from "dva";

const dataLastWeek={
    id: 20,
    title: '上周周报',
    content: '第20周周报',
    date: '2019.05.13-2019.05.19',
    focus: [
          {
              title: '香港通报福克兰群岛进口冷藏白鳕鱼样本汞含量超标',
              content: '香港食环署食安中心3月19日公布，一个由福克兰群岛进口的冷藏白鳕鱼样本被检出金属杂质汞（俗称水银）的含量超出法例标准。中心通过食物监察计划从荃湾一商户抽取上述样本进行检测，结果显示样本含0.83ppm的汞，超出法例标准的0.5ppm。发言人表示，中心已知会涉事商户上述违规情况，并正追查有关产品的来源及分销情况。',
              link: 'https://www.fehd.gov.hk/sc_chi/news/details/20190319_7381.html'
          },
          {
              title: '家乐福等超市涉嫌售卖假冒星巴克咖啡',
              content: '人民网北京3月19日报道，近期多家超市售卖贴有“广州百益”字样防伪标签的假冒速溶咖啡。北京市朝阳区、东城区两家家乐福超市、广和里中街一家BC进口商品折扣商行、朝阳区西大望路一小区超市等多家超市都在售被媒体曝光的同款“星巴克速溶咖啡”，每盒五条装，价格32元至52元不等。星巴克方面表示，相关免煮咖啡系列使用“星巴克VIA”商标，均为进口商品，目前只在星巴克线下门店和星巴克天猫旗舰店有售，其他渠道均为未经授权的产品。目前警方已查封一生产窝点。',
              link: 'http://consume.people.com.cn/n1/2019/0319/c425315-30983616.html'
          }
      ],
    moreInformation: [],
    appendix: [
        {
            key: '1',
            country: '美国',
            publish: 'FDA',
            date: '2019-03-16',
            enterprise: '/',
            brand: '/',
            origin: '/',
            nameAndDescription: '黑橄榄或腌黄瓜，带有阿拉伯字符，标有FRO AlahmadABNORRT？LJSHAMINA，保质期为2020-02-20',
            unqualifiedReason: '品质',
        },
        {
            key: '2',
            country: '意大利',
            publish: 'MDS',
            date: '2019-03-16',
            enterprise: 'IKEA',
            brand: 'IKEA',
            origin: '/',
            nameAndDescription: '棉花糖蛋糕，规格180g，产品编号00229026保质期2019-03-12至2019-04-18',
            unqualifiedReason: '致敏原',
        },
    ]
  };
const dataToday= {
        id: 142,
        title: '本日日报',
        content: '第142期日报',
        date: '2019.05.22',
        focus: [
              {
                  title: '香港通报福克兰群岛进口冷藏白鳕鱼样本汞含量超标',
                  content: '香港食环署食安中心3月19日公布，一个由福克兰群岛进口的冷藏白鳕鱼样本被检出金属杂质汞（俗称水银）的含量超出法例标准。中心通过食物监察计划从荃湾一商户抽取上述样本进行检测，结果显示样本含0.83ppm的汞，超出法例标准的0.5ppm。发言人表示，中心已知会涉事商户上述违规情况，并正追查有关产品的来源及分销情况。',
                  link: 'https://www.fehd.gov.hk/sc_chi/news/details/20190319_7381.html'
              },
              {
                  title: '家乐福等超市涉嫌售卖假冒星巴克咖啡',
                  content: '人民网北京3月19日报道，近期多家超市售卖贴有“广州百益”字样防伪标签的假冒速溶咖啡。北京市朝阳区、东城区两家家乐福超市、广和里中街一家BC进口商品折扣商行、朝阳区西大望路一小区超市等多家超市都在售被媒体曝光的同款“星巴克速溶咖啡”，每盒五条装，价格32元至52元不等。星巴克方面表示，相关免煮咖啡系列使用“星巴克VIA”商标，均为进口商品，目前只在星巴克线下门店和星巴克天猫旗舰店有售，其他渠道均为未经授权的产品。目前警方已查封一生产窝点。',
                  link: 'http://consume.people.com.cn/n1/2019/0319/c425315-30983616.html'
              }
          ],
        moreInformation: [
              {
                  title: '2018年度婴幼儿配方乳粉行业发展分析报告发布',
                  content: ' 2019年3月20日，中食安信发布2018年度婴幼儿配方乳粉行业发展分析报告。该报告对 2018 年中国婴幼儿配方乳粉行业的市场运行状况、法规标准以及政府监管情况等进行了汇总分析，报告内容主要包括：2018 年婴幼儿配方乳粉行业市场发展状况、2018 年发布的相关法规标准汇总及分析、2018 年婴幼儿配方乳粉质量状况汇总及分析、2018 年婴幼儿配方乳粉行业的舆情状况及重点企业的发展动态等。',
                  link: 'https://mp.weixin.qq.com/s/MDCfKQm6CXXfwZ6KnROWqw'
              },
              {
                  title: '成都学校食堂“过期食品”调查结果：粉条不合格 多人涉谣被查',
                  content: '人民网北京3月19日电（孝金波 实习生张琳）针对近日发生的“成都七中实验学校食堂事件”，18日晚八点左右，成都市温江区市场监督管理局官方微博将成都七中实验学校食堂食材第二批13个样品检测结果进行通报。该校送检的调料酱、毛肚、辣椒粉、牛排等12种食材的检测结论均为：“所检项目符合食品安全标准要求”，只有此前被曝光发霉的粉条，被检测出“有霉斑，不合格”。',
                  link: 'https://baijiahao.baidu.com/s?id=1628597183728484258&wfr=spider&for=pc'
              },
              {
                  title: '南京海关立案侦办440吨走私冻品案件',
                  content: '近日，南京海关立案侦办一起走私冻品案件，现场查扣走私船舶1艘、冻品16个集装箱，约440吨，抓获走私犯罪嫌疑人10名。  3月10日夜间11点，在江苏盐城灌河某民用码头，有一艘轮船正在卸载集装箱，情况异常。南京海关所属盐城海关缉私分局接到线索后，立即启动联勤联动机制，联合公安边防和海事部门水陆联动，一方面对岸上正在作业的装卸人员进行控制，另一方面对已经闻讯出逃的轮船进行追击，经过一个多小时的追赶，于11日凌晨在灌河口将其拦截。',
                  link: 'http://www.js.xinhuanet.com/2019-03/18/c_1124237104.htm'
              },
              {
                  title: '台湾地区“农委会”发布2月大米抽检情况',
                  content: '中新网2月18日电 据台湾“中央社”报道，台湾彰化县顺弘牧场出现芬普尼(氟虫腈)问题鸡蛋事件，外界批评1月25日就抽样，检验结果却到2月13日才公布，问题蛋早被民众吃下肚。台湾“农委会”2月18日表示，未来将要求送验后48小时内公布检验结果。',
                  link: 'http://www.sohu.com/a/295432080_162758'
              },
          ],
        appendix: [
            {
                key: '1',
                country: '美国',
                publish: 'FDA',
                date: '2019-03-16',
                enterprise: '/',
                brand: '/',
                origin: '/',
                nameAndDescription: '黑橄榄或腌黄瓜，带有阿拉伯字符，标有FRO AlahmadABNORRT？LJSHAMINA，保质期为2020-02-20',
                unqualifiedReason: '品质',
            },
            {
                key: '2',
                country: '意大利',
                publish: 'MDS',
                date: '2019-03-16',
                enterprise: 'IKEA',
                brand: 'IKEA',
                origin: '/',
                nameAndDescription: '棉花糖蛋糕，规格180g，产品编号00229026保质期2019-03-12至2019-04-18',
                unqualifiedReason: '致敏原',
            },
        ]
    };
@connect(({report, loading}) => ({
    report,
    loading: loading.effects['report/fetch'],
}))
class DailyReport extends Component {
    //从url获取id，防止刷新丢失id
    reportId=this.props.match.params.id;

  //   //返回按钮点击事件，返回/report/basic
  //   onBackChange = (id) => {
  //   const { match } = this.props;
  //   router.push({pathname: `${match.url}`.replace('/'+id,'')});
  // };
    constructor(props){
        super(props);
        this.export = React.createRef();
    }
    onExportPdf=()=>{
        html2canvas(this.export.current).then((canvas) => {
              let contentWidth = canvas.width;
              let contentHeight = canvas.height;
              console.log(contentWidth,contentHeight);
              //一页pdf显示html页面生成的canvas高度;
              let pageHeight = contentWidth / 592.28 * 841.89;
              //未生成pdf的html页面高度
              let leftHeight = contentHeight ;
              //页面偏移
              let position = 0;
              //a4纸的尺寸[595.28,841.89]，html页面生成的canvas在pdf中图片的宽高
              let imgWidth = 595.28;
              let imgHeight = 592.28/contentWidth * contentHeight;

              let pageData = canvas.toDataURL('image/jpeg', 1.0);
              let pdf = new jsPDF('', 'pt', 'a4');

              //有两个高度需要区分，一个是html页面的实际高度，和生成pdf的页面高度(841.89)
              //当内容未超过pdf一页显示的范围，无需分页
              if (leftHeight < pageHeight) {
              pdf.addImage(pageData, 'JPEG', 0, 0, imgWidth, imgHeight );
              } else {
                  while(leftHeight > 0) {
                      pdf.addImage(pageData, 'JPEG', 0, position, imgWidth, imgHeight);
                      leftHeight -= pageHeight;
                      position -= 841.89;
                      //避免添加空白页
                      if(leftHeight > 0) {
                        pdf.addPage();
                      }
                  }
              }

              pdf.save('content.pdf');
          }
        );
    };

    componentDidMount() {
        const {dispatch} = this.props;
        this.reqRef = requestAnimationFrame(() => {
            dispatch({
                type: 'report/fetchDetail',
                payload: this.reportId
            });
        });
    }

    componentWillUnmount() {
        const {dispatch} = this.props;
        dispatch({
            type: 'report/clear',
            payload: this.state.reportType
        });
        cancelAnimationFrame(this.reqRef);
    }

    render() {
        const {report, loading} = this.props;
        const {reportDetail} = report;

        const volDailySum=2091;
        const volWeekSum=52;
        const columns = [
            {
                title: '国家区域',
                dataIndex: 'country',
                key: 'country',
                align: 'center',
                width: '100px'
            },
            {
                title: '发布机构',
                dataIndex: 'publish',
                key: 'publish',
                align: 'center',
                width: '100px'
            },
            {
                title: '发布日期',
                dataIndex: 'date',
                key: 'date',
                align: 'center',
                width: '150px'
            },
            {
                title: '企业',
                dataIndex: 'enterprise',
                key: 'enterprise',
                align: 'center',
            },
            {
                title: '品牌',
                dataIndex: 'brand',
                key: 'brand',
                align: 'center',
            },
            {
                title: '原产地',
                dataIndex: 'origin',
                key: 'origin',
                align: 'center',
                width: '100px'
            },
            {
                title: '产品名称及描述',
                dataIndex: 'nameAndDescription',
                key: 'nameAndDescription',
                align: 'center',
            },
            {
                title: '不合格原因',
                dataIndex: 'unqualifiedReason',
                key: 'unqualifiedReason',
                align: 'center',
                width: '120px'
            },
        ];

        return(
            <div ref={this.export}>
                <Row style={{fontSize:'20px'}}>
                    {/*<Row gutter={24}>*/}
                    {/*    <Col span={20} offset={2}>*/}
                    {/*        <Icon type="left-circle" theme="twoTone" style={{fontSize:'30px'}} onClick={this.onBackChange.bind(this,id)}/>&emsp;返回*/}
                    {/*    </Col>*/}
                    {/*</Row>*/}

                    {/*<Divider></Divider>*/}

                    <Row gutter={24}>
                        <Col span={16} offset={4}>
                            <Card>
                                <Row>
                                    <Button onClick={this.onExportPdf.bind(this)}>导出</Button>
                                </Row>

                                <Divider/>

                                <Row style={{textAlign: 'center',fontSize:'40px'}}>{dataToday.moreInformation.length === 0?'食 品 安 全 信 息 周 报' : '食 品 安 全 信 息 日 报'}</Row>
                                <Divider style={{fontSize:'20px'}}>
                                    第&nbsp;<b>{id}</b>&nbsp;期&emsp;(总&nbsp;<b>{dataToday.moreInformation.length === 0? volWeekSum : volDailySum}</b>&nbsp;期)
                                </Divider>
                                <br/>
                                <Row gutter={24}>
                                    <Col span={16} offset={4}>
                                        <Row gutter={24}>
                                            <Col span={8}>进出口食品安全局</Col>
                                            <Col span={8} offset={8} style={{textAlign: 'right',fontSize:'20px'}}>
                                                <p>{dataToday.date}</p>
                                            </Col>
                                        </Row>
                                        <Divider/>
                                        <div style={{textAlign: 'center',fontSize:'25px'}}>目录</div>
                                        <List
                                            header={<div>【重点关注】</div>}
                                            split={false}
                                            dataSource={dataToday.focus}
                                            renderItem={item => (
                                                <List.Item>
                                                    {item.title}
                                                </List.Item>
                                            )}
                                        />
                                        {dataToday.moreInformation.length === 0?'':
                                            <List
                                            header={<div>【更多信息】</div>}
                                            split={false}
                                            dataSource={dataToday.moreInformation}
                                            renderItem={item => (
                                                <List.Item>
                                                    {item.title}
                                                </List.Item>
                                            )}
                                        />}
                                        <Divider/>
                                        <List
                                            header={<div>【重点关注】</div>}
                                            dataSource={dataToday.focus}
                                            renderItem={item => (
                                                <List.Item>
                                                    {item.title}<br/><br/>
                                                    {item.content}<br/><br/>
                                                    {item.link}
                                                </List.Item>
                                            )}
                                        />
                                        <Divider/>
                                        {dataToday.moreInformation.length === 0 ? '':
                                            <List
                                            header={<div>【更多信息】</div>}
                                            dataSource={dataToday.moreInformation}
                                            renderItem={item => (
                                                <List.Item>
                                                    {item.title}<br/><br/>
                                                    {item.content}<br/><br/>
                                                    {item.link}
                                                </List.Item>
                                            )}
                                        />}


                                    </Col>
                                </Row>
                                <Divider/>
                                <Row>
                                    <p>【附件】</p>
                                    <p style={{textAlign: 'center', fontSize: '20px'}}>境外食品化妆品预警信息({dataToday.date})</p><br/>
                                    <Table
                                        bordered
                                        dataSource={dataToday.appendix}
                                        columns={columns}
                                        pagination={false}/>
                                </Row>
                            </Card>
                        </Col>
                    </Row>
                </Row>
            </div>
        )
    };
}

export default DailyReport;