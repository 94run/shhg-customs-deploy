import React, { PureComponent } from 'react';
import {List, Row, Col, Descriptions, Card} from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import styles from './Articles.less';
import Link from 'umi/link';


const statusMap = {'Y':'已处理', 'N': '待处理', 'F': '失效'};
const articleExtMap = {'enterprise': '企业',
    'brand': '品牌',
    'originArea': '原产地',
    'productName': '产品名称及描述',
    'releaseInst': '不合格原因',
    'measure': '预警召回'};

@connect(({ feedbackList, loading }) => ({
    feedbackList,
    feedbackListLoading: loading.effects['feedbackList/fetch'],
}))
class Center extends PureComponent{

    componentDidMount() {
         const { dispatch } = this.props;
         dispatch({
             type: 'feedbackList/fetch',
             payload: {
                 currentPage: 1,
                 pageSize: 10
             }
         });
    }

    handleListChange(page,pageSize) {
        const { dispatch } = this.props;
         dispatch({
             type: 'feedbackList/fetch',
             payload: {
                 currentPage: page,
                 pageSize: pageSize
             }
         });
    }

    /**
     * 渲染预警召回类舆情的表格
     */
    renderDescriptionItems(articleExt){
        if (articleExt === null) {
        articleExt = {
            enterprise: '',
            brand: '',
            originArea: '',
            productName: '',
            riskItem: '',
            measure: ''
        }
    }
        articleExt = (({enterprise, brand, originArea, productName, releaseInst, measure}) => (
            {enterprise, brand, originArea, productName, releaseInst, measure}))(articleExt);
        return Object.keys(articleExt).map(item =>(
                <Descriptions.Item span={1} label={articleExtMap[item]}>{articleExt[item]}</Descriptions.Item>
        ))
    }

    static formatContent(content) {
        if (!content) {
            return "";
        }
        return content.replace(/(\r\n|\n|\r|\\n)/g, "<br/>");
    }

    render(){
        const {feedbackList, feedbackListLoading} = this.props;
        const {
            list,
            pagination,
        } = feedbackList;

        const paginationProps = {
            current: pagination.currentPage === undefined ? 1 : pagination.currentPage,
            pageSize: pagination.pageSize === undefined ? 10 : pagination.pageSize,
            total: pagination.total === undefined ? 10 : pagination.total,
            onChange: (page,pageSize) => {this.handleListChange(page,pageSize)}
        };

        return (
            <Card bordered={false} loading={feedbackListLoading}>
                <List
                size="large"
                itemLayout="vertical"
                className={styles.articleList}
                dataSource={list}
                pagination={paginationProps}
                renderItem={item => (
                  <List.Item key={item.feedbackId}>
                    <GridContent>
                        <Row gutter={24} style={{marginLeft: '40px'}}>
                          <Col span={16}>{statusMap[item.status]}</Col>
                          <Col span={4} offset={4}><em>{moment(item.gmtCreate).format('YYYY-MM-DD HH:mm')}</em></Col>
                        </Row>
                        <Row gutter={24}>
                          <Col span={22} offset={1}>
                            <Row style={{marginLeft: '40px'}}>
                              <Row>反馈信息：{item.content}</Row>
                              <Row style={{marginBottom: '12px', marginTop: '12px'}}>
                                  {item.article && item.article.articleType !== 'yjhzh'  && <div className={styles.listContent}>
                                      <Link className={styles.articleSummary} to={'/check/detail/' + item.article.articleId}>
                                          <div className={styles.description} dangerouslySetInnerHTML={{
                                              __html: Center.formatContent(item.article.summary),
                                          }}/>
                                      </Link>
                                  </div>}
                                  {item.article && item.article.articleType === 'yjhzh' &&
                                  <Descriptions column={1} bordered size="small" className={styles.customsDescriptions}>
                                      {this.renderDescriptionItems(item.article.articleExt)}</Descriptions>}
                              </Row>
                            </Row>
                          </Col>
                        </Row>
                    </GridContent>
              </List.Item>
                )}
            />
            </Card>
        )
    }
}

export default Center;
