import React, {PureComponent} from 'react';
import {connect} from 'dva';
import router from 'umi/router';
import {Card, Row, Col, Icon, Avatar} from 'antd';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import styles from './Center.less';
import avatar from '@/assets/avatar.png'

@connect(({loading, user, project}) => ({
    currentUser: user.currentUser,
    currentUserLoading: loading.effects['user/fetchCurrent'],
    project,
}))

class Center extends PureComponent {
    state = {
        newTags: [],
        inputVisible: false,
        inputValue: '',
        key: 'Likes',
    };

    componentDidMount() {
        const {dispatch} = this.props;
        dispatch({
            type: 'user/fetchCurrent',
        });
        dispatch({
            type: 'user/connectSocket',
        });

        dispatch({
            type: 'list/fetch',
            payload: {
                count: 8,
            },
        });
    }

    onTabChange = (key, type) => {
        this.setState({ [type]: key });
        const {match} = this.props;
        switch (key) {
            case 'Likes':
                router.push(`${match.url}/likes`);
                break;
            case 'FeedBacks':
                router.push(`${match.url}/feedBacks`);
                break;
            default:
                break;
        }
    };

    render() {
        const {
            currentUser,
            children,
        } = this.props;
        const operationTabList = [
            {
                key: 'Likes',
                tab: (
                    <span>
                        <Icon type="like-o" style={{fontSize: 18}}/>
                        我的点赞
                    </span>
                ),
                style: {marginLeft: 45}
            },
            {
                key: 'FeedBacks',
                tab: (
                    <span>
                        <Icon type="warning" style={{fontSize: 18}}/>
                        我的反馈
                    </span>
                ),
            },
        ];

        return (
                <GridContent className={styles.userCenter}>
                    <Row>
                        <Col span={18} offset={3}>
                            <Card style={{paddingLeft: 20}}>
                                <Row gutter={24}>
                                    <Col span={2} offset={1}>
                                        <Avatar src={avatar} size={64}/>
                                    </Col>
                                    <Col span={4}>
                                            <div className={styles.name}>{currentUser.username}</div>&emsp;&emsp;
                                            <div>{currentUser.institution}</div>
                                    </Col>
                                </Row>
                            </Card>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={18} offset={3}>
                            <Card
                                className={styles.tabsCard}
                                bordered={false}
                                tabList={operationTabList}
                                activeTabKey={this.state.key}
                                onTabChange={key => {
                                    this.onTabChange(key,'key');
                                }}
                            >
                                {children}
                            </Card>
                        </Col>
                    </Row>
                </GridContent>
        );
    }
}

export default Center;
