import {queryFeedbackList} from "@/services/api";

export default {
    namespace: 'feedbackList',

    state: {
        list: [],
        pagination: {}
    },

    effects: {
        * fetch({payload}, {call, put}) {
            const response = yield call(queryFeedbackList, payload);
            yield put({
                type: 'queryList',
                payload: response.data,
            });
        },
        * fetchDefault({payload}, {call, put}) {
            const response = yield call(queryFeedbackList, payload);
            yield put({
                type: 'queryList',
                payload: Array.isArray(response) ? response : [],
            });
        },
    },

    reducers: {
        queryList(state, { payload }) {
            return {
                ...state,
                list: payload.data,
                pagination: payload.pagination
            };
        }
    },
};
