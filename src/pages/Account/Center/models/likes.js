import {queryLikeList} from '@/services/api';

export default {
    namespace: 'likes',

    state: {
        list: [],
        pagination: {},
        likePJ: []
    },

    effects: {
        * queryLikeList({payload}, {call, put}) {
            const response = yield call(queryLikeList, payload);
            yield put({
                type: 'queryLikeReducer',
                payload: response.data,
            });
        },
        * fetchDefault({payload}, {call, put}) {
            const response = yield call(queryLikeList, payload);
            yield put({
                type: 'queryList',
                payload: Array.isArray(response) ? response : [],
            });
        },
    },

    reducers: {
        queryLikeReducer(state, { payload }) {
            return {
                ...state,
                list: payload.data,
                pagination: payload.pagination
            };
        },
    },
};
