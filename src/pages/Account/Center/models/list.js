import { queryLikeList, queryFeedbackList, addFakeList, updateFakeList, queryFakeListOne } from '@/services/api';

export default {
  namespace: 'list1',

  state: {
    likeList: [],
    feedbackList: [],
    detail: {}
  },

  effects: {
    *fetchLikeList({ payload }, { call, put }) {
      const response = yield call(queryLikeList, payload);
      yield put({
        type: 'queryList',
        payload: Array.isArray(response) ? response : [],
      });
    },
    *fetchFeedbackList({ payload }, { call, put }) {
      const response = yield call(queryFeedbackList, payload);
      yield put({
        type: 'queryList',
        payload: Array.isArray(response) ? response : [],
      });
    },
    *fetchOne({ payload }, { call, put }) {
      const response = yield call(queryFakeListOne, payload);
      yield put({
        type: 'queryListOne',
        payload: response
      });
    },
    *appendFetch({ payload }, { call, put }) {
      const response = yield call(queryFakeList, payload);
      yield put({
        type: 'appendList',
        payload: Array.isArray(response) ? response : [],
      });
    },
    *submit({ payload }, { call, put }) {
      let callback;
      if (payload.id) {
        callback = Object.keys(payload).length === 1 ? removeFakeList : updateFakeList;
      } else {
        callback = addFakeList;
      }
      const response = yield call(callback, payload); // post
      yield put({
        type: 'queryList',
        payload: response,
      });
    },
  },

  reducers: {
    queryList(state, action) {
      return {
        ...state,
        list: action.payload,
      };
    },
    queryListOne(state, action) {
      return {
        ...state,
        detail: action.payload,
      };
    },
    appendList(state, action) {
      return {
        ...state,
        list: state.list.concat(action.payload),
      };
    },
  },
};
