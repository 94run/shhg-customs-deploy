import React, {PureComponent} from 'react';
import {List, Row, Card} from 'antd';
import {connect} from 'dva';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import styles from './Articles.less';
import ArticleHeader from "../../../components/ArticleHeader";

@connect(({likes, feedback, loading}) => ({
    likes,
    feedback,
    likeListLoading: loading.effects['likes/queryLikeList'],
}))
class Center extends PureComponent {
    componentDidMount() {
        const {dispatch} = this.props;
        dispatch({
            type: 'likes/queryLikeList',
            payload: {
                currentPage: 1,
                pageSize: 10
            }
        });
         dispatch({
            type: 'feedback/queryFeedbackTypeList',
        });
    }

    handleListChange(page, pageSize) {
        const {dispatch} = this.props;
        dispatch({
            type: 'likes/queryLikeList',
            payload: {
                currentPage: page,
                pageSize: pageSize
            }
        });
    }

    render() {
        const {likes, likeListLoading, feedback: {articleProperties},} = this.props;
        const {
            list,
            pagination
        } = likes;

        const paginationProps = {
            current: pagination.currentPage === undefined ? 1 : pagination.currentPage,
            pageSize: pagination.pageSize === undefined ? 10 : pagination.pageSize,
            total: pagination.total === undefined ? 10 : pagination.total,
            onChange: (page, pageSize) => {
                this.handleListChange(page, pageSize)
            }
        };

        return (
            <Card bordered={false} loading={likeListLoading}>
                <List
                size="large"
                itemLayout="vertical"
                className={styles.articleList}
                dataSource={list}
                pagination={paginationProps}
                renderItem={item => (
                    <List.Item key={item.id}>
                        <GridContent>
                            <Row gutter={24}>
                                <ArticleHeader data={item} articleProperties={articleProperties} disableOperate={true}/>
                            </Row>
                        </GridContent>
                    </List.Item>
                )}
            />
            </Card>

        )
    }
}

export default Center;
