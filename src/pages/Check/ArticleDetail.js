import React, {PureComponent, Component} from 'react';
import {List, Avatar, Tag, Icon, Card, Pagination, Row, Col, Empty, Button} from 'antd';
import {connect} from 'dva';
import moment from 'moment';
import ArticleListContent from '@/components/ArticleListContent';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
// import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import router from 'umi/router';
import ArticleHeader from '@/components/ArticleHeader';
import styles from './ArticleDetail.less';


@connect(({loading, feedback, articles}) => ({
    articles,
    feedback,
    articleLoading: loading.effects['articles/queryArticle'],
}))
class ArticleDetail extends Component {
    state = {
        articleId: 0
    }

    componentDidMount() {
        const pms = {
            id: this.props.match.params.id
        };
        const {dispatch} = this.props;
        dispatch({
            type: 'articles/queryArticle',
            payload: {
                fromBack: false,
                articleId: pms.id
            },
            callback: (resp) => {
                if (!resp) {
                    return;
                }
                dispatch({
                    type: 'articles/queryLikesByArticleIds',
                    payload: {articleIds: resp.articleId}
                });
        }
    });
    dispatch({
            type: 'feedback/queryFeedbackTypeList',
        });
        this.setState({
            articleId: pms.id
        })
    }

    static formatContent(content) {
        if (!content) {
            return "";
        }
        return content.replace(/(\r\n|\n|\r|\\n)/g, "<br/>");
    }

    goBack(){
        router.goBack()
    }

    render() {
        const {
            articles: {article, likeMap, likeMapSearch},
            feedback: {articleProperties},
            articleLoading
        } = this.props;

        return (
            <GridContent className={styles.articleWrapper}>
                <Row>
                    <Button type="link" size='large' onClick={this.goBack}><Icon type="left" />返回</Button>
                </Row>
                <Row gutter={24}>
                    <ArticleHeader data={article} loading={articleLoading} articleProperties={articleProperties}
                                   likeItem={likeMap[article.articleId]}/>
                </Row>
                <Row type="flex" justify="center">
                    <Col lg={16} md={16} offset={4} className={styles.articleDetail}>
                        <Card bordered={false} loading={articleLoading}>
                            <Col lg={4} md={20} className={styles.tagsTitle}>
                                重点词句
                            </Col>
                            <Col lg={16} md={20} >
                                <div className={styles.tags}>
                                    {
                                        article.keywords && article.keywords.map(k => <Tag key={k}>{k}</Tag>)
                                    }
                                </div>
                            </Col>
                        </Card>
                        <Card bordered={false} style={{marginTop: 24}} loading={articleLoading}>
                            <Col lg={4} md={20} className={styles.tagsTitle}>
                                原文
                            </Col>
                            <Col lg={16} md={20}>
                                <p><b>{article.title}</b>
                                </p>
                                <div className={styles.articleList} dangerouslySetInnerHTML={{
                                    __html: ArticleDetail.formatContent(article.content),
                                }}/>
                                <div style={{marginTop: '20px'}}>
                                    { article.articleExt &&
                                        <p className={styles.docTbStyle} dangerouslySetInnerHTML={{__html: article.articleExt.tableHtmlList}}/>
                                    }
                                </div>
                            </Col>
                        </Card>
                    </Col>
                </Row>
            </GridContent>
        )
    }
}

export default ArticleDetail;
