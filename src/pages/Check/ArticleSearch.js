import React, {PureComponent} from 'react';
import {connect} from 'dva/index';
import router from 'umi/router';
import styles from "./Center/Center.less";
import {Affix, BackTop, Button, Card, Col, Icon, Input, Row, Tag} from "antd";
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import Articles from "./Center/Articles";

const Search = Input.Search;

@connect(({loading, user, type, articles}) => ({
    listLoading: loading.effects['articles/queryByConditions'],
    currentUser: user.currentUser,
    currentUserLoading: loading.effects['user/fetchCurrent'],
    type,
    articles,
    articleTypeLoading: loading.effects['type/queryArticleTypes'],
}))
class ArticleSearch extends PureComponent {
    state = {
        articleType: 'all',
        title: '',
        hoverType: '',
        backgroundColor: '',
        flag: 'search'
    };

    componentDidMount() {
        const {dispatch, articles: {articlesSearch, conditionSearch}} = this.props;
        if (JSON.stringify(conditionSearch) !== '{}') {
            this.setState({
                articleType: conditionSearch.articleType,
                title: conditionSearch.title
            });
        }
        if (articlesSearch && articlesSearch.length > 0) {
            return;
        }
        dispatch({
            type: 'user/fetchCurrent',
        });

        this.searchByConditions();
        dispatch({
            type: 'type/queryArticleTypes',
            payload: {
                flag: 'search',
            }
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.articleType !== prevState.articleType || this.state.title !== prevState.title) {
            document.documentElement.scrollTop = 0;
        }
    }

    handleSearch = (val, type) => {
        const {dispatch} = this.props;
        const {flag} = this.state;
        if (type === 'title') {
            this.setState({title: val}, () => {
                this.searchByConditions()
            });
        }
        if (type === 'articleType') {
            this.setState({articleType: val}, () => {
                this.searchByConditions()
            });
        }
        const {title, articleType} = this.state;

        dispatch({
            type: 'articles/putCondition',
            payload: {
                condition: {
                    title: type === 'title' ? val : title,
                    articleType: type === 'articleType' ? val : articleType,
                    flag: flag,
                }
            }
        })
    }

    async searchByConditions() {
        const {dispatch} = this.props;
        const {title, articleType, flag} = this.state;
        let param = {
            currentPage: 1,
            pageSize: 15,
            title: title,
            flag: flag,
        };
        if (articleType !== 'all') {
            param = {
                ...param,
                articleType: articleType
            }
        }

        dispatch({
            type: 'articles/queryByConditions',
            payload: param,
            callback: (resp) => {
                if (!resp) {
                    return;
                }
                const articleIds = [];
                resp.forEach(item => {
                    articleIds.push(item.articleId);
                });
                dispatch({
                    type: 'articles/queryLikesByArticleIds',
                    payload: {
                        articleIds: (articleIds).join(","),
                        flag: flag,
                    }
                });
            }
        });
    }

    static ChangeArticleTypeList(list) {
        if (list.length === 0) {
            return list;
        } else {
            const all = {code: 'all', description: '全部', orders: 0};
            if (!list.find((element) => (element.orders === 0))) {
                list.unshift(all);
            } else {
                list[0] = all;
            }
            return list;
        }
    }

    mouseOver = (val) => {
        this.setState({
            hoverType: val,
            backgroundColor: '#F0F2F5'
        })

    }

    mouseOut = (val) => {
        this.setState({
            hoverType: val,
            backgroundColor: ''
        })
    }

    static goBack() {
        router.goBack()
    }

    render() {
        const {
            listLoading,
            articleTypeLoading,
            articles: {paginationSearch},
        } = this.props;
        let {type: {articleTypesSearch}} = this.props;
        articleTypesSearch = ArticleSearch.ChangeArticleTypeList(articleTypesSearch);

        const {articleType, flag, title} = this.state;
        return (
            <GridContent>
                <Row style={{marginBottom: '12px'}}>
                    <Button type="link" size='large' onClick={ArticleSearch.goBack}><Icon type="left"/>返回</Button>
                </Row>
                <Row style={{marginBottom: '12px'}} gutter={24}>
                    <Col lg={{span: 10, offset: 6}} md={{span: 24, offset: 12}}>
                        <Search
                            placeholder={this.state.title}
                            onSearch={value => this.handleSearch(value, 'title')}
                            style={{marginBottom: '12px'}}
                        /><br/>
                        <div>共{paginationSearch.total}条搜索结果</div>
                    </Col>
                </Row>
                <Row gutter={24}>
                    <Col lg={4} md={12}>
                        <Affix offsetTop={10}>
                            <Card bordered={false} loading={articleTypeLoading}
                                  bodyStyle={{padding: 24, marginBottom: 24}}>
                                <div>
                                    {
                                        articleTypesSearch.map(articleType => (
                                            <a key={articleType.code}
                                               onClick={this.handleSearch.bind(this, articleType.code, 'articleType')}
                                               onMouseOver={this.mouseOver.bind(this, articleType.code)}
                                               onMouseOut={this.mouseOut.bind(this, articleType.code)}
                                               style={{
                                                   fontWeight: articleType.code === this.state.articleType ? "bold" : "normal",
                                                   fontSize: articleType.code === this.state.articleType ? "18px" : "16px",
                                                   backgroundColor: articleType.code === this.state.hoverType ? this.state.backgroundColor : '',
                                                   marginBottom: 20, display: 'block', textAlign: 'center'
                                               }}
                                            >
                                                {articleType.description}
                                            </a>
                                        ))
                                    }
                                </div>
                            </Card>
                        </Affix>
                    </Col>
                    <Col lg={15} md={24}>
                        <Card
                            className={styles.tabsCard}
                            bordered={false}
                            loading={listLoading}>
                            <Articles conditions={{articleType, flag, title}}/>
                        </Card>
                    </Col>
                </Row>
                <BackTop style={{right: '350px'}} visibilityHeight={800}/>
            </GridContent>
        );
    }
}

export default ArticleSearch;
