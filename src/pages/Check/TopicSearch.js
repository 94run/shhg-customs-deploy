import React, {PureComponent} from 'react';
import {connect} from 'dva/index';
import router from 'umi/router';
import styles from "./Center/Center.less";
import {Affix, BackTop, Button, Card, Col, Icon, Row} from "antd/lib/index";
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import Articles from "./Center/Articles";

@connect(({loading, user, type, articles}) => ({
    type,
    articles,
    currentUser: user.currentUser,
    listLoading: loading.effects['articles/queryByConditions'],
    currentUserLoading: loading.effects['user/fetchCurrent'],
    keywordsLoading: loading.effects['type/queryHotKeywords'],
}))
class TopicSearch extends PureComponent {
    state = {
        title: '',
        hoverType: '',
        backgroundColor: '',
        flag: 'topic',
    };

    componentDidMount() {
        const {dispatch, articles: {articlesTopic, conditionTopic}} = this.props;
        if(!this.props.location.state){
            router.push('./center');
            return;
        }
        const {topic} = this.props.location.state;
        if(topic){
            this.setState({title: topic}, () => {
                this.searchByConditions()
            });
             dispatch({
                 type: 'type/queryHotKeywords',
             });
            return;
        }
        if (JSON.stringify(conditionTopic) !== '{}') {
            this.setState({
                title: conditionTopic.title
            });
        }
        if (articlesTopic && articlesTopic.length > 0) {
            return;
        }
        dispatch({
            type: 'user/fetchCurrent',
        });
        dispatch({
            type: 'type/queryHotKeywords',
        });
        this.searchByConditions();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.title !== prevState.title) {
            document.documentElement.scrollTop = 0;
        }
    }

    handleSearch = (val, type) => {
        const {dispatch} = this.props;
        if (type === 'title') {
            this.setState({title: val}, () => {
                this.searchByConditions()
            });
        }
        const {title, flag} = this.state;

        dispatch({
            type: 'articles/putCondition',
            payload: {
                condition: {
                    title: type === 'title' ? val : title,
                    flag: flag,
                }
            }
        })
    };

    async searchByConditions() {
        const {dispatch} = this.props;
        const {title, flag} = this.state;
        let param = {
            currentPage: 1,
            pageSize: 15,
            title: title,
            flag: flag,
        };
        dispatch({
            type: 'articles/queryByConditions',
            payload: param,
            callback: (resp) => {
                if (!resp) {
                    return;
                }
                const articleIds = [];
                resp.forEach(item => {
                    articleIds.push(item.articleId);
                });
                dispatch({
                    type: 'articles/queryLikesByArticleIds',
                    payload: {
                        articleIds: (articleIds).join(","),
                        flag: flag,
                    }
                });
            }
        });
    }

    mouseOver = (val) => {
        this.setState({
            hoverType: val,
            backgroundColor: '#F0F2F5'
        })

    };

    mouseOut = (val) => {
        this.setState({
            hoverType: val,
            backgroundColor: ''
        })
    };

    static goBack() {
        router.goBack()
    }

    render() {
        const {
            listLoading,
            keywordsLoading,
            articles: {paginationTopic},
            type: {keywords},
        } = this.props;

        const {title, hoverType, backgroundColor, flag} = this.state;
        return (
            <GridContent>
                <Row style={{marginBottom: '12px'}}>
                    <Button type="link" size='large' onClick={TopicSearch.goBack}><Icon type="left"/>返回</Button>
                </Row>
                <Row style={{marginBottom: '12px'}} gutter={24}>
                    <Col lg={{span: 10, offset: 4}} md={{span: 24, offset: 12}} style={{fontSize: '18px'}}>
                        #{title}#&emsp;&emsp;共计{paginationTopic.total}条
                    </Col>
                </Row>
                <Row gutter={24}>
                    <Col lg={4} md={12}>
                        <Affix offsetTop={10}>
                            <Card bordered={false} style={{marginBottom: 24, padding: 0}} loading={keywordsLoading}>
                            <div className={styles.subject}>
                                {keywords.map(item => (
                                    <a key={item.name}
                                       onClick={this.handleSearch.bind(this, item.name, 'title')}
                                       onMouseOver={this.mouseOver.bind(this, item.name)}
                                       onMouseOut={this.mouseOut.bind(this, item.name)}
                                       style={{
                                           fontWeight: item.name === title ? "bold" : "normal",
                                           fontSize: item.name === title ? "18px" : "16px",
                                           backgroundColor: item.name === hoverType ? backgroundColor : '',
                                           marginBottom: 20, display: 'block', textAlign: 'center'
                                       }}
                                    >
                                        #{item.name}#
                                    </a>
                                ))
                                }
                            </div>
                        </Card>
                        </Affix>
                    </Col>
                    <Col lg={15} md={24}>
                        <Card
                            className={styles.tabsCard}
                            bordered={false}
                            loading={listLoading}>
                            <Articles conditions={{title, flag}}/>
                        </Card>
                    </Col>
                </Row>
                <BackTop style={{right: '350px'}} visibilityHeight={800}/>
            </GridContent>
        );
    }
}

export default TopicSearch;
