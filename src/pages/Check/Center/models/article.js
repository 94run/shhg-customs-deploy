import {queryArticleList, removeRule, addRule, updateRule, queryArticleDetail, queryArticleTypeList, queryArticleByCondition} from '@/services/api';
import {findPos} from '@/utils/utils';

export default {
    namespace: 'article',

    state: {
        data: {
            list: [],
            pagination: {},
        },
        articleTypeList: [],
    },

    effects: {
        * queryArticleList({payload}, {call, put}) {
            const response = yield call(queryArticleList, payload);
            yield put({
                type: 'queryReducer',
                payload: response.data,
            });
        },
        * queryArticleTypeList({payload}, {call, put}) {
            const response = yield call(queryArticleTypeList, payload);
            yield put({
                type: 'queryArticleTypeReducer',
                payload: {articleTypeList: response.data.data},
            });
        },
        * queryArticleDetail({payload}, {call, put}) {
            const response = yield call(queryArticleDetail, payload);
            yield put({
                type: 'queryDetailReducer',
                payload: {
                    article: response.data.data,
                },
            });
        },
        * queryArticleByCondition({payload}, {call, put}) {
            const response = yield call(queryArticleByCondition, payload);
            yield put({
                type: 'queryReducer',
                payload: response.data,
            });
        },
        * add({payload, callback}, {call, put}) {
            const response = yield call(addRule, payload);
            yield put({
                type: 'save',
                payload: response,
            });
            if (callback) callback();
        },
        * remove({payload, callback}, {call, put}) {
            const response = yield call(removeRule, payload);
            yield put({
                type: 'save',
                payload: response,
            });
            if (callback) callback();
        },
        * update({payload, callback}, {call, put}) {
            const response = yield call(updateRule, payload);
            yield put({
                type: 'save',
                payload: response,
            });
            if (callback) callback();
        },
    },

    reducers: {
        save(state, action) {
            return {
                ...state,
                data: action.payload,
            };
        },
        queryReducer(state, {payload}) {
            const {data} = state;
            return {
                ...state,
                data: {
                    ...data,
                    list: payload.data,
                    pagination: payload.pagination
                }
            }
        },
        queryArticleTypeReducer( state, {payload: articleTypeList}){
            return {
                ...state,
                ...articleTypeList
            }
        },
        queryDetailReducer(state, {payload:article}) {
            return {
                ...state,
                ...article,
            }
        },
        addReducer(state, {payload: item}) {
            const {data} = state;
            data['list'].splice(0, 0, item.item);
            return {...state, data: data}
        },
        updateReducer(state, {payload: item}) {
            const {data} = state;
            const pos = findPos(item.item, data['list']);
            if (pos >= 0) {
                data['list'].splice(pos, 1, item.item);
            }
            return {...state, data: data}
        },
        removeReducer(state, {payload: item}) {
            const {data} = state;
            const pos = findPos(item.item, data['list']);
            if (pos >= 0) {
                data['list'].splice(pos, 1);
            }
            return {...state, data: data}
        },
    },
};
