import React, {PureComponent} from 'react';
import {List} from 'antd';
import {connect} from 'dva';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import styles from './Articles.less';
import ArticleHeader from "@/components/ArticleHeader";

@connect(({articles, feedback}) => ({
    articles,
    feedback,
}))
class Articles extends PureComponent {

    state = {
        modalVisible: false,
        articleId: 0,
        formState: [],
        checkboxTypeList: []
    };

    /**
     * 从后台PoPropertyEnum中加载反馈表单需要的属性值
     * 获取喜欢的列表
     */
    componentDidMount() {
        const {dispatch} = this.props;
        dispatch({
            type: 'feedback/queryFeedbackTypeList',
        });
    }

    handleListChange(page, pageSize) {
        const {country, productType, riskItem, title, articleType, flag} = this.props.conditions;
        const {dispatch} = this.props;
        dispatch({
            type: 'articles/queryByConditions',
            payload: {
                currentPage: page,
                pageSize: pageSize,
                country: !country || country.code === undefined ? '' : country.code,
                title: !title ? '' : title,
                productType: !productType || productType.code === undefined ? '' : productType.code,
                riskItem: !riskItem || riskItem.code === undefined ? '' : riskItem.code,
                articleType: !articleType || articleType === 'all' ? '' : articleType,
                flag: flag
            },
            callback: (resp) => {
                if (!resp) {
                    return;
                }
                const articleIds = [];
                resp.forEach(item => {
                    articleIds.push(item.articleId);
                });
                dispatch({
                    type: 'articles/queryLikesByArticleIds',
                    payload: {articleIds: (articleIds).join(",")}
                });
            }
        });
        document.documentElement.scrollTop = 0;
    }

    selectDataUse(flag){
        let {
            articles: {articles, pagination, likeMap, likeMapSearch, articlesSearch, paginationSearch, articlesTopic, paginationTopic}
        } = this.props;
        let data;
        switch (flag) {
            case 'center':
                data = {
                    articlesUse: articles,
                    paginationUse: pagination,
                    likeMapUse: likeMap
                };
                break;
            case 'search':
                data = {
                    articlesUse: articlesSearch,
                    paginationUse: paginationSearch,
                    likeMapUse: likeMapSearch
                };
                break;
            case 'topic':
                data = {
                    articlesUse: articlesTopic,
                    paginationUse: paginationTopic,
                    likeMapUse: likeMap
                };
                break
        }
        return data
    }

    render() {
        const {flag} = this.props.conditions;
        const {
            feedback: {articleProperties},
        } = this.props;

        const data = this.selectDataUse(flag);

        const paginationProps = {
            current: data.paginationUse.current === undefined ? 1 : parseInt(data.paginationUse.current),
            pageSize: data.paginationUse.pageSize === undefined ? 10 : parseInt(data.paginationUse.pageSize),
            total: data.paginationUse.total === undefined ? 10 : parseInt(data.paginationUse.total),
            onChange: (page, pageSize) => {
                this.handleListChange(page, pageSize)
            }
        };

        return (
            <div>
                <List
                    size="large"
                    itemLayout="vertical"
                    className={styles.articleList}
                    dataSource={data.articlesUse}
                    pagination={paginationProps}
                    renderItem={item => (
                        <List.Item key={item.articleId}>
                            <GridContent>
                                <ArticleHeader key={item.articleId} data={item} articleProperties={articleProperties}
                                               likeItem={data.likeMapUse[item.articleId]} flag={flag}/>
                            </GridContent>
                        </List.Item>
                    )}
                />
            </div>
        )
    }
}

export default Articles
