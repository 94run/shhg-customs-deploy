import React, {PureComponent} from 'react';
import {connect} from 'dva';
import Link from 'umi/link';
import router from 'umi/router';
import {Card, Row, Col, Icon, Avatar, Tag, Divider, Spin, Input, List, Affix, BackTop, message, Alert} from 'antd';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import styles from './Center.less';
import Articles from './Articles'

const Search = Input.Search;

@connect(({loading, user, project, type, articles}) => ({
    listLoading: loading.effects['articles/queryByConditions'],
    currentUser: user.currentUser,
    currentUserLoading: loading.effects['user/fetchCurrent'],
    project,
    articles,
    type,
    projectLoading: loading.effects['project/fetchNotice'],
    articleTypeLoading: loading.effects['type/queryArticleTypes'],
    countriesLoading: loading.effects['type/queryCountries'],
    riskItemsLoading: loading.effects['type/queryRiskItems'],
    productTypesLoading: loading.effects['type/queryProductTypes'],
}))
class Center extends PureComponent {
    state = {
        newTags: [],
        inputVisible: false,
        inputValue: '',
        articleType: 'jrrd',
        country: {},
        productType: {},
        riskItem: {},
        showAllCountry: false,
        showSearchBar: true,
        backgroundColor: '',
        hoverType: '',
        flag: 'center'
    };

    componentDidMount() {
        const {dispatch, articles: {articles, condition}} = this.props;
        if (JSON.stringify(condition) !== '{}') {
            this.setState({
                country: condition.country,
                productType: condition.productType,
                riskItem: condition.riskItem,
                articleType: condition.articleType
            });
        }
        if (articles && articles.length > 0) {
            return;
        }
        dispatch({
            type: 'user/fetchCurrent',
        });

        this.searchByConditions();
        dispatch({
            type: 'type/queryArticleTypes',
            payload: {
                flag: 'center'
            }
        });
        dispatch({
            type: 'type/queryCountries',
        });
        dispatch({
            type: 'type/queryRiskItems',
        });
        dispatch({
            type: 'type/queryHotKeywords',
        });
        dispatch({
            type: 'type/queryProductTypes',
        });
        dispatch({
            type: 'project/fetchNotice',
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.articleType !== prevState.articleType || this.state.riskItem !== prevState.riskItem
            || this.state.country !== prevState.country || this.state.productType !== prevState.productType) {
            document.documentElement.scrollTop = 0;
        }
    }

    handleSearch = (val, type) => {
        const {dispatch} = this.props;
        const {flag} = this.state;
        if (type === 'country') {
            this.setState({country: val}, () => {
                this.searchByConditions()
            });
        }
        if (type === 'riskItem') {
            this.setState({riskItem: val}, () => {
                this.searchByConditions()
            });
        }
        if (type === 'productType') {
            this.setState({productType: val}, () => {
                this.searchByConditions()
            });
        }
        if (type === 'articleType') {
            this.setState({articleType: val}, () => {
                this.searchByConditions()
            });
        }
        const {country, riskItem, productType, articleType} = this.state;

        dispatch({
            type: 'articles/putCondition',
            payload: {
                condition: {
                    country: type === 'country' ? val : country,
                    riskItem: type === 'riskItem' ? val : riskItem,
                    productType: type === 'productType' ? val : productType,
                    articleType: type === 'articleType' ? val : articleType,
                    flag: flag
                }
            }
        })

    };

    async searchByConditions() {
        const {dispatch} = this.props;
        const {country, riskItem, productType, title, articleType, flag} = this.state;
        dispatch({
            type: 'articles/queryByConditions',
            payload: {
                currentPage: 1,
                pageSize: 15,
                country: country.code,
                title: title,
                productType: productType.code,
                riskItem: riskItem.code,
                articleType: articleType,
                flag: flag
            },
            callback: (resp) => {
                if (!resp) {
                    return;
                }
                const articleIds = [];
                resp.forEach(item => {
                    articleIds.push(item.articleId);
                });
                dispatch({
                    type: 'articles/queryLikesByArticleIds',
                    payload: {
                        articleIds: (articleIds).join(","),
                        flag: flag,
                    }
                });
            }
        });
    };

    handleShowAllCountry = () => {
        const {showAllCountry} = this.state;
        this.setState({
            showAllCountry: !showAllCountry,
        });
    };

    handleShowSearchBar = () => {
        const {showSearchBar} = this.state;
        this.setState({
            showSearchBar: !showSearchBar,
        });
    };

    mouseOver = (val) => {
        this.setState({
            hoverType: val,
            backgroundColor: '#F0F2F5'
        })

    };

    mouseOut = (val) => {
        this.setState({
            hoverType: val,
            backgroundColor: ''
        })
    };

    renderCountries = () => {
        const {showAllCountry} = this.state;
        const {
            type: {countries},
        } = this.props;
        if (showAllCountry) {
            return countries.map(item => (
                <Tag key={item.code} className={styles.tag}
                     onClick={this.handleSearch.bind(this, item, 'country')}>{item.description}</Tag>
            ));
        } else {
            return countries.slice(0, 30).map(item => (
                <Tag key={item.code} className={styles.tag}
                     onClick={this.handleSearch.bind(this, item, 'country')}>{item.description}</Tag>
            ));
        }
    };

    static ChangeArticleTypeList(list) {
        if (list.length === 0) {
            return list;
        }
        const dailyHot = {code: 'jrrd', description: '今日热点', orders: 0};
        if (!list.find((element) => (element.orders === 0))) {
            list.unshift(dailyHot);
        } else {
            list[0] = dailyHot;
        }
        return list;
    }

    render() {
        const {
            listLoading,
            currentUserLoading,
            articleTypeLoading,
            countriesLoading,
            productTypesLoading,
            riskItemsLoading,
            type: {productTypes, riskItems, keywords},
            articles: {newArticleNum}
        } = this.props;
        let {type: {articleTypes}} = this.props;
        articleTypes = Center.ChangeArticleTypeList(articleTypes);

        const {country, productType, riskItem, showAllCountry, showSearchBar, articleType, flag} = this.state;

        return (
            <GridContent className={styles.userCenter}>
                <Row gutter={24}>
                    <Col lg={4} md={12}>
                        <Affix offsetTop={10}>
                            <Card bordered={false} loading={articleTypeLoading}
                                  bodyStyle={{padding: 24, marginBottom: 24}}>
                                <div>
                                    {
                                        articleTypes.map(articleType => (
                                            <a key={articleType.code}
                                               onClick={this.handleSearch.bind(this, articleType.code, 'articleType')}
                                               onMouseOver={this.mouseOver.bind(this, articleType.code)}
                                               onMouseOut={this.mouseOut.bind(this, articleType.code)}
                                               style={{
                                                   fontWeight: articleType.code === this.state.articleType ? "bold" : "normal",
                                                   fontSize: articleType.code === this.state.articleType ? "18px" : "16px",
                                                   backgroundColor: articleType.code === this.state.hoverType ? this.state.backgroundColor : '',
                                                   marginBottom: 20,
                                                   display: 'block',
                                                   textAlign: 'center'
                                               }}>{articleType.description}
                                            </a>
                                        ))
                                    }
                                </div>
                            </Card>
                        </Affix>
                    </Col>
                    <Col lg={15} md={24}>
                        {newArticleNum > 0 &&
                        <a onClick={this.searchByConditions.bind(this)} style={{marginBottom: 10}}>
                            <Alert
                                message={"有" + newArticleNum + "条新舆情，点击查看！"}
                                className={styles.clickRefresh}
                                type="info"
                                showIcon
                            />
                        </a>}
                        <Card
                            className={styles.tabsCard}
                            bordered={false}
                            loading={listLoading}>
                            {productType && JSON.stringify(productType) !== "{}" &&
                            <Tag closable className={styles.tag}
                                 onClose={this.handleSearch.bind(this, {}, 'productType')}>{productType.description}</Tag>}
                            {country && JSON.stringify(country) !== "{}" && <Tag closable className={styles.tag}
                                                                                 onClose={this.handleSearch.bind(this, {}, 'country')}>{country.description}</Tag>}
                            {riskItem && JSON.stringify(riskItem) !== "{}" && <Tag closable className={styles.tag}
                                                                                   onClose={this.handleSearch.bind(this, {}, 'riskItem')}>{riskItem.description}</Tag>}
                            <Articles conditions={{country, productType, riskItem, articleType, flag}}/>
                        </Card>
                    </Col>
                    <Col lg={5} md={12}>
                        <Card bordered={false} style={{marginBottom: 0}} loading={currentUserLoading}>
                            <div className={styles.search}>
                                {!showSearchBar && <Icon type="search" onClick={this.handleShowSearchBar}/>}
                                {showSearchBar && <Search
                                    placeholder="关键字搜索"
                                    onSearch={() => router.push({pathname: '/check/articleSearch'})}
                                    onClick={() => router.push({pathname: '/check/articleSearch'})}
                                />}
                            </div>
                        </Card>

                        <Card bordered={false} style={{marginBottom: 24, padding: 0}} loading={currentUserLoading}>
                            <div className={styles.subject}>
                                {keywords.map(item => (
                                    <p>
                                        <Link to={{pathname: '/check/topicSearch', state: {topic: item.name}}}>#{item.name}#</Link>
                                    </p>
                                ))
                                }
                            </div>
                        </Card>
                        <Card bordered={false} style={{marginBottom: 24}} loading={productTypesLoading}>
                            <div className={styles.tags}>
                                <div className={styles.tagsTitle}>产品类型</div>
                                {productTypes.map(item => (
                                    <Tag key={item.code} className={styles.tag}
                                         onClick={this.handleSearch.bind(this, item, 'productType')}>{item.description}</Tag>
                                ))}
                            </div>
                        </Card>

                        <Card bordered={false} style={{marginBottom: 24}} loading={countriesLoading}>
                            <div className={styles.tags}>
                                <div className={styles.tagsTitle}>国家区域</div>
                                {this.renderCountries()}

                                <div className={styles.downCircle} onClick={this.handleShowAllCountry}>
                                    {!showAllCountry && <Icon
                                        type="down-circle"/>}
                                    {showAllCountry && <Icon
                                        type="up-circle"/>}
                                </div>
                            </div>
                        </Card>

                        <Card bordered={false} style={{marginBottom: 24}} loading={riskItemsLoading}>
                            <div className={styles.tags}>
                                <div className={styles.tagsTitle}>风险项</div>
                                {riskItems.map(item => (
                                    <Tag key={item.code} className={styles.tag}
                                         onClick={this.handleSearch.bind(this, item, 'riskItem')}>{item.description}</Tag>
                                ))}
                            </div>
                        </Card>
                    </Col>
                </Row>
                <BackTop style={{right: '350px'}} visibilityHeight={800}/>
            </GridContent>);
    }
}

export default Center;
